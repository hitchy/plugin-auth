---
layout: home

hero:
  name: "@hitchy/plugin-auth"
  text: authentication & authorization
  tagline: |
    easily integrated with your Hitchy-based application
  actions:
    - theme: brand
      text: About
      link: ./introduction.md
    - theme: brand
      text: Getting started
      link: ./guides/getting-started.md
    - theme: brand
      text: API reference
      link: ./api/index.md
    - theme: brand
      text: SAML example
      link: ./guides/saml.md
    - theme: brand
      text: OIDC example
      link: ./guides/openid-connect.md
---

### MIT License

Copyright &copy; 2024 cepharum GmbH

:::details Click to see the full text ...
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
:::
