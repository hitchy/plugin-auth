---
prev: ../introduction.md
next: false
---

# Guides

This section provides simple tutorials explaining how to work with this plugin:

* [Getting started](getting-started.md)
* [Authorization](authorization.md)
* [OpenID Connect](openid-connect.md)
* [SAML 2.0](saml.md)
