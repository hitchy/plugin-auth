---
prev: ../guides/
next: authorization.md
---

# Quick Start

This document is a step-by-step guide for basically adding support for authentication and authorization to your [Hitchy](https://core.hitchy.org/)-based application using this plugin.

## Setup Project

Create a folder for your application and enter it:

```bash
mkdir my-new-app
cd my-new-app
```

Initialize your application's package management so dependencies are tracked:

```bash
npm init -y
```

Add [@hitchy/core](https://www.npmjs.com/package/@hitchy/core), [@hitchy/plugin-auth](https://www.npmjs.com/package/@hitchy/plugin-auth) and the latter one's dependencies:

```bash
npm install @hitchy/core @hitchy/plugin-auth @hitchy/plugin-odem @hitchy/plugin-cookies @hitchy/plugin-session
```

This plugin maintains a user's authentication in a server-side session controlled by a cookie included with succeeding requests. Users, roles and their authorizations are managed in a connected datasource. Thus, these additional plugins must be installed, too.

## Start Hitchy

At this point Hitchy is ready. Thus, start it with:

```bash
hitchy start
```

:::warning Hitchy not found?
If Hitchy is not found this might be due to issues with your Node.js and npm setup. In most cases using **npx** solves this issue for now:

```bash
npx hitchy start
```
:::

Keep it running while trying out next.

:::tip
Whenever you want to stop Hitchy press Ctrl+C to gracefully shut it down.
:::

## Try It Out

The plugin [injects special endpoints for managing a user's authentication](../api/routing.md) by default. You should be able to authenticate with an HTTP request like this:

```http request
POST /api/auth/login HTTP/1.0
Content-Type: application/x-www-form-urlencoded

username=admin&password=nimda
```

This works due to the default [configuration](../api/config.md#config-auth-admin) asking to implicitly [set up an administrator account](/api/service/auth-manager.md#createadminifmissing) if missing on application start.

## Next steps

There are additional guides you might want to check out next:

* [Authentication with SAML](saml.md)
* [Authentication with OIDC](openid-connect.md)
* [Authorization control](authorization.md)
