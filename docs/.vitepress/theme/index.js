import DefaultTheme from "vitepress/theme";

import "./style.css";

export default {
	extends: DefaultTheme,
	async enhanceApp( ctx ) {} // eslint-disable-line no-empty-function,no-unused-vars
};
