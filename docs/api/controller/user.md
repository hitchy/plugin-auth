---
prev: ../controller/
next: ../policy/
---

# UserController

This controller provides request handlers basically suitable for authenticating as a user. However, neither handler is actually triggering a user's authentication or its logout, but provides suitable responses following some [additionally required policies](../policy/user.md).

Any of the following request handlers can be used as target in your application's routing declarations in [section `routes`](https://core.hitchy.org/api/hitchy.html#config-routes) of [runtime configuration](https://core.hitchy.org/api/hitchy.html#configuration):

```json
{
    "routes": {
        "/login": "user.authenticate",
        "/logout": "user.unauthenticate"
    }
}
```

## authenticate()

This handler is responding on successful authentication. 

It relies on policy [authentication.login](../policy/authentication.md#login) being processed, first. 

## unauthenticate()

This handler is responding on successfully dropping any authentication. 

It relies on policy  [authentication.logout](../policy/authentication.md#logout) being processed, first. 

## changePassword()

This handler is responding on successfully changing user's token. 

It relies on policy [user.changePassword](../policy/user.md#changepassword) being processed, first. 

## getCurrent()

This handler is delivering additional information on recently authenticated user. See related [routing description for response examples](../routing.md#get-api-auth-current).

It does not depend on any additional policy being processed first. 

