---
prev: config.md
next: false
---

# Routing defaults

Unless [disabled in runtime configuration](config.md#config-auth-prefix), this plugin is always setting up routes providing basic user authentication to a client.

::: warning Customizable prefix  
The following documentation relies on default prefix `/api/auth`. You may [change this default in runtime configuration](config.md#config-auth-prefix). Adopt URLs listed below according to your setup if required.  
:::

Some basic initialization is required in every request. This can not be disabled using runtime configuration.

## `POST /api/auth/login{/:strategy}`

This route is authenticating some user by processing its credentials found in request body which is either JSON-formatted or some URL-encoded form data. 

Actual format of provided credentials depend on passport strategy being used. The built-in local strategy used by default expects `username` and `password` being provided in properties named accordingly:

```http request
POST /api/auth/login HTTP/1.0
Content-Type: application/x-www-form-urlencoded

username=john.doe&password=my-secret-token
```

The result is always provided as JSON object.

:::tip Background
This routing combines policy [authentication.login](policy/authentication.md#login) with controller [user.authenticate](controller/user.md#authenticate).
:::

## `GET /api/auth/login/:strategy`

This route is integrating authentication code for optionally picking up external authentication on returning from remote service. You need to provide a strategy here.

## `GET /api/auth/logout`

This route is triggering removal any server-side session data suitable for implicitly re-authenticating some current user. 

The result is provided as JSON object.

:::tip Background
This routing combines policy [authentication.logout](policy/authentication.md#logout) with controller [user.unauthenticate](controller/user.md#unauthenticate).
:::

## `GET /api/auth/current`

The route delivers information on some currently authenticated user as JSON object.

If no user is authenticated, the result is:

```json
{
	"success": true,
    "authenticated": false
}
```

Following a successful authentication, the result is similar to this one:

```json
{
	"success": true,
    "authenticated": {
        "uuid": "12345678-1234-1234-1234-1234-123456789012",
        "name": "john.doe",
        "roles": ["users", "customers"]
    }
}
```

:::tip Background
This routing targets controller [user.getCurrent](controller/user.md#getcurrent).
:::

## `POST /api/auth/password`

This route is processing request for changing some previously authenticated user's token a.k.a. password. 

All information is read from request body which is either JSON-formatted or some URL-encoded form data. It must contain properties `current` and `next` providing user's current password and the one to become user's next password.

```http request
POST /api/auth/password HTTP/1.0
Content-Type: application/x-www-form-urlencoded

current=my-old-secret&next=my-new-secret
```

The result is provided as JSON object.

::: warning Current user, only  
By intention, this request handler is limited to changing current user's password, only. A custom request handler is required to change password of different users.
:::

:::tip Background
This routing combines policy [user.changePassword](policy/user.md#changepassword) with controller [user.changePassword](controller/user.md#changepassword).
:::

