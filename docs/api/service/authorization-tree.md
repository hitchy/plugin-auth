---
prev: auth-manager.md
next: authorization-node.md
---

# AuthorizationTree

This service implements class for tracking state of authorizations by means of granting or revoking access on named resources to/from users and role in a tree of [nodes](authorization-node.md). 

In opposition to other services, this service is primarily a class which could be instantiated to describe a tree of authorization settings. It complies with existing services by [exposing a single instance as current tree of authorizations](#current), only.

On starting application, this plugin is processing runtime configuration and local database for existing [authorization rules](../model/authorization-rule.md) building this tree in runtime memory for improved authorizations testing during request routing.

## Properties

### current

This static property exposes single instance of current runtime representing the tree which has been set up by the plugin on starting application. This instance is implicitly updated by some actions affecting existing users, roles and authorization rules in local database.

## Methods

::: warning Instance methods  
In opposition to other services, these methods are instance methods and thus are available e.g. on current tree, only.

Instead of writing

```javascript
AuthorizationTree.addRule( rule )
```

you must write

```javascript
AuthorizationTree.current.addRule( rule )
```
:::

### selectNode()

**Signature:** `selectNode( selector, [ addIfMissing ], [ callback ] ): AuthorizationNode`

This method descends into tree of nodes according to provided selector which is a resource's hierarchical name. If optional argument `addIfMissing` is `true`, any missing node is created on the fly assuring to always return a node representing selected resource.

When providing callback in third argument, it is invoked on every existing (or implicitly created) node passed while descending into tree. The callback is invoked with 

* current node, 
* segment of selector a.k.a. resource name selecting this node in context of its parent, 
* the segment's index in list of segments extracted from selector and
* the full list of segments to be processed.

The callback may return `false` to prematurely stop descending into tree.

### clear()

**Signature:** `clear(): AuthorizationTree`

Clears tree by recursive deleting all its nodes. Returns tree itself for daisy-chaining calls.

### addRule()

**Signature:** `addRule( rule ): AuthorizationTree`

Adjusts tree to represent provided [authorization rule](../model/authorization-rule.md). The provided rule may be an instance of model [AuthorizationRule](../model/authorization-rule.md) or any other object resembling authorization rules by providing equivalent properties `selector`, `user`, `role` and `accept`.

::: warning Note  
Adding a rule does not imply to eventually grant access on some resource.  
:::

The method returns current tree for daisy-chaining calls.

### removeRule()

**Signature:** `removeRule( rule ): AuthorizationTree`

This is the counterpart of [addRule()](#addrule). It adjusts tree to stop representing provided [authorization rule](../model/authorization-rule.md).

::: warning Note  
Removing a rule does not imply to eventually revoke access on some resource. It's reverting some previous change of tree, only.  
:::

::: warning Note  
When adding a rule multiple times, it will be counted as such. You need to remove the rule as many times as it has been added before to actually remove it from tree.    
:::

The method returns current tree for daisy-chaining calls.

### isAuthorized()

**Signature:** `isAuthorized( selector, userName, roleNames, [ acceptByDefault ] ): boolean`

This method checks, whether tree is currently granting or revoking access on a selected resource to/from user and/or role.

::: warning Note  
Access on a resource can be granted or revoked on particular resource. It might be granted/revoked on a superordinated resource, too.

The method keeps descending into the tree looking for the most specific rule granting access to a named user or role or revoking it accordingly. Eventually, the result closest to the given selector is considered.  
:::

The method takes the name(s) of one or more users and the name(s) of one or more roles and checks if it either provided name is addressed by any authorization rule tracked for a node of the tree that is passed on descending into the tree according to provided selector. If there is no rule on any passed node addressing any named user or role, the default provided in optional fourth argument is used, which is false by default, thus preventing access by default.

The method returns `true`, if access is granted, and `false` otherwise.

### gc()

**Signature:** `gc( [ force ] ): void`

This method is meant to search tree for sparse threads and remove them if necessary. It is implicitly invoked each time a rule is removed with [removeRule()](#removerule), though garbage collection is triggered after seeing a certain number of calls, only. You can enforce a garbage collection by setting optional argument `force`.

### loadFromDatabase()

**Signature:** `async loadFromDatabase(): void`

This method is reading all existing instances of [AuthorizationRule](../model/authorization-rule.md) from local database invoking [addRule()](#addrule) on every found rule.

:::info
This method is invoked by the plugin's initialization code. Thus, any existing item of model **AuthorizationRule** is considered automatically next time your application is restarted.
:::

### loadFromConfiguration()

**Signature:** `loadFromConfiguration( configuration ): void`

The method searches [provided configuration](../config.md#config-auth-authorizations) for describing rules in one of several supported formats and adds them to current tree. The provided configuration can be

* an object mapping resource names into
  * a string naming a user granted or revoked access on named resource,
  * a string consisting of a leading `@` followed by a role's name access on named resource is granted to or revoked from,
  * `*` or `@*` for granting or revoking access to/from any user or any role on named resource,
  * a string naming multiple users and/or roles accordingly, each separated by a comma,
  * an array of either sort of string or
  * an object providing `users` and `roles` or `grants` and `revokes` in separate properties each supporting a format similar to the one of strings described above and another map of relative resource names into rules for resources subordinated to current one.

:::info
This method is invoked automatically by the plugin's initialization code. Changes to your application's runtime configuration are considered next time your application is restarted.
:::
