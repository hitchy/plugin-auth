---
prev: authorization-node.md
next: ../config.md
---

# Authorization

This service is implements convenient helpers for testing a given user's authorization.

## Methods

### mayAccess( user, resources, checkAll )

**Signature:** `mayAccess( user, resources, checkAll = false ): Promise<boolean>`

Tests if selected user is granted access to one or all named resources and provides a promise for the result.

Argument `user` is passed to [AuthManager.asUser()](auth-manager.md#asuser) method to get the [user's record](../model/user.md) including its roles' names. Because of that, parameter `user` may be a user's name used to fetch the according user's model item or that fetched item itself.

Argument `resources` is

* a string naming a single resource or
* an array of such resource names.

The method returns a promise resolved with `true` if the given user is granted access 

* on at least one of the named resources (if `checkAll` is falsy or omitted) or 
* on all named resources (if `checkAll` is truthy).

Otherwise the promise is resolved with `false`.

Access is tested using [current tree of authorization rules](authorization-tree.md#current).
