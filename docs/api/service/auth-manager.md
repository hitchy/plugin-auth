---
prev: authentication-strategies.md
next: authorization-tree.md
---

# AuthManager

This service provides several helper functions commonly useful on managing authentication and authorization at runtime. 

::: tip FYI  
Due to supporting both parts, the name is reduced to common prefix _Auth_ by intention.  
:::

## Properties

### adminRole

This property commonly exposes name of role implicitly granting access to any resource without regards to existing authorization rules in runtime configuration or in local database. The resulting name depends on current runtime environment and runtime configuration.

## Methods

### asUser()

**Signature:** `async asUser( user, [ createIfMissing ] ): User`

This wrapper is meant to assure that provided user is actually an instance of model [User](../model/user.md) existing in local database. 

It takes a user's name, ...

```javascript
const user = await AuthManager.asUser( "john.doe" );
```

... an object with property name selecting desired user (ignoring any other property in that object) ...

```javascript
const user = await AuthManager.asUser( { name: "john.doe", foo: "bar" } );
```

... or the user's existing instance as input.

```javascript
const user = await AuthManager.asUser( new User( uuid ) );
```

It retrieves instance of user selected either way. Due to accepting instances of [User](../model/user.md), it can be safely used multiple times.

Optional boolean parameter `createIfMissing` must be set true to implicitly create selected user in local database if missing. Otherwise, the method throws an error causing promised result to be rejected.

### asRole()

**Signature:** `async asRole( role, [ createIfMissing ] ): Role`

This is the counterpart to [asUser()](#asuser) for simplifying access on roles. The same semantics apply.

### listRolesOfUser()

**Signature:** `async listRolesOfUser( user, [ createIfMissing ], [ uuidsOnly ] ): Roles[]`

The method takes a user and qualifies it via [asUser()](#asuser) before fetching list of user's associated roles. Argument for optional `createIfMissing` is forwarded to `asUser()` and `false` by default. Argument for optional `uuidsOnly` must be set explicitly to prevent listed instances of [Role](../model/role.md) to implicitly load their records from database.

### listUsersOfRole()

**Signature:** `async listUsersOfRole( role, [ createIfMissing ], [ uuidsOnly ] ): User[]`

This method is the counterpart to [listRolesOfUser()](#listrolesofuser): it is querying local database for users associated with a given role. Provided role is qualified via [asRole()](#asrole) with argument `createIfMissing` being forwarded. 

Optional boolean parameter `uuidsOnly` must be set to prevent implicit retrieval of either listed user's record from database.

### grantRoleToUser()

**Signature:** `async grantRoleToUser( role, user, [ createIfMissing] ): void`

This helper is provided for conveniently [associating](../model/user-to-role.md) a [user](../model/user.md) with a [role](../model/role.md). Provided arguments for `role` and `user` are qualified with [asRole()](#asrole) and [asUser()](#asuser). Optional argument for `createIfMissing` is forwarded to either method internally.

### revokeRoleFromUser()

**Signature:** `async revokeRoleFromUser( role, user, [ createIfMissing] ): void`

This helper is provided for conveniently removing any existing [association](../model/user-to-role.md) between a [user](../model/user.md) and a [role](../model/role.md). Provided arguments for `role` and `user` are qualified with [asRole()](#asrole) and [asUser()](#asuser). Optional argument for `createIfMissing` is forwarded to either method internally.

### createAdminIfMissing()

**Signature:** `async createAdminIfMissing(): void`

This method is used during bootstrap to make sure there is always a user associated with configured [administrator role](#adminrole) preventing any authorization management from locking out all existing users.
