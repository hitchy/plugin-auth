---
prev: authentication-passport.md
next: auth-manager.md
---

# AuthenticationStrategies

This service helps with picking a default strategy. In addition, it exposes a strategy for local authentication. The latter is used on integrating [Passport](https://www.passportjs.org/) while bootstrapping your application and this plugin.

## Methods

### defaultStrategy()

**Signature:** `defaultStrategy(): string`

Provides name of strategy to use by default depending on current application's configuration.

### generateLocal()

**Signature:** `generateLocal(): Strategy`

Creates custom instance of Passport's LocalStrategy set up for integrating with Hitchy's request routing.

### generateSaml()

**Signature:** `generateSaml( name: string, configuration ): Promise<Strategy>`

Creates a Passport strategy suitable for integrating a SAML-based authentication with Hitchy's request routing. The first argument is the name under which the strategy will be provided in this plugin's configuration. The second argument is a configuration object specific to the SAML authentication of the 3rd-party library [passport-saml](https://www.passportjs.org/packages/passport-saml/#config-parameter-details) which is used internally. 

See [the related example](../../guides/saml.md) for additional information.

:::warning Signature has changed!  
Prior to version v0.5, this method was returning the resulting strategy directly. Starting with v0.5, a promise for that strategy is returned instead.  
:::

### generateOpenIdConnect()

**Signature:** `generateOpenIdConnect( name: string, configuration ): Promise<Strategy>`

Creates a Passport strategy suitable for integrating an OIDC-based authentication with Hitchy's request routing. The first argument is the name under which the strategy will be provided in this plugin's configuration. The second argument is a configuration object specific to the OIDC authentication of the 3rd-party library [openid-client](https://www.passportjs.org/packages/openid-client/). The URL provided in property `discovery_url` is queried to discover the authenticating service's configuration. After that, a client for that service with the remainder of configuration provided here. 

See [the related example](../../guides/openid-connect.md) for additional information.
