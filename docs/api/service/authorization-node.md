---
prev: authorization-tree.md
next: authorization-policy-generator.md
---

# AuthorizationNode

This class is primarily used by [AuthorizationTree](authorization-tree.md) to describe a tree of nodes.

## Properties

### parent

Refers to parent authorization node in a tree of nodes. It is nullish on top-most node of tree.

### name

Provides segment of resource name used to address this node in context of its parent node.

## Methods

### clear()

**Signature:** `clear(): AuthorizationNode`

Clears thread of current node by recursively invoking this very method on superordinated nodes before deleting any list of users and roles tracked in context of current node for being granted or revoked access on resource represented by current node.

Returns this node for daisy-chaining calls.

### addRole()

**Signature:** `addRole( roleName, [ accept ] ): AuthorizationNode`

Adds named role to current node for being granted or revoked access depending on optionally provided argument `accept`. On omitting or on providing `true`, access on resource represented by current node is granted to named role. Otherwise, access is revoked.

Returns this node for daisy-chaining calls.

:::tip  
Special name `*` is supported as wildcard for matching any role.  
:::

### addUser()

**Signature:** `addUser( userName, [ accept ] ): AuthorizationNode`

Adds named user to current node for being granted or revoked access depending on optionally provided argument `accept`. On omitting or on providing `true`, access on resource represented by current node is granted to named user. Otherwise, access is revoked.

Returns this node for daisy-chaining calls.

:::tip  
Special name `*` is supported as wildcard for matching any user.  
:::

### removeRole()

**Signature:** `removeRole( roleName, [ accept ] ): AuthorizationNode`

Revokes previously granted or revoked access on resource represented by current node to/from named role. Optionally provided argument `accept` must be equivalent to what was provided on adding role before.

Returns this node for daisy-chaining calls.

:::tip  
Special name `*` is supported as wildcard for matching any role. Using it does not remove all existing roles from current node, but removes `*` as a special role name added before.  
:::

### removeUser()

**Signature:** `removeUser( userName, [ accept ] ): AuthorizationNode`

Revokes previously granted or revoked access on resource represented by current node to/from named user. Optionally provided argument `accept` must be equivalent to what was provided on adding role before.

Returns this node for daisy-chaining calls.

:::tip  
Special name `*` is supported as wildcard for matching any user. Using it does not remove all existing users from current node, but removes `*` as a special username added before.  
:::

### getChild()

**Signature:** `getChild( name, [ addIfMissing ] ): AuthorizationNode`

Retrieves reference on node subordinated to current one representing immediate child of current resource in a hierarchy of resources.

Provided `name` must be a single segment's name, only. Optional argument `addIfMissing` must be `true` to always return another node. Otherwise, undefined is returned if selected child node does not exist.

### isAuthorized()

**Signature:** `isAuthorized( userName, roleName ): number`

Checks if current nodes' lists of grants and revokes for accessing represented resource are containing provided user name or role name. 

The method returns

* -1 if user/role is revoked access on represented resource,
* 1 if user/role is granted access on represented resource and
* 0 if current node does not affect provided user or role.

If either provided name of a user or role is granted access while the other is rejected, the method throws exception.

### isSpare()

**Signature:** `isSpare(): boolean`

Detects if current node and all of its child nodes are spare by means of not granting or revoking access on their represented resource to/from any user or role.

This information is used on garbage collection to drop data in runtime memory which is not actually required anymore.

### gc()

**Signature:** `gc(): AuthorizationNode`

Runs garbage collection on thread of current node returning a reduced copy of current node's thread for replacement in scope of parent node. `undefined` is returned in case of current node being available for removal.

### path()

**Signature:** `path(): string`

Recursively compiles qualified name of resource represented by current node.
