---
prev: ../
next: false
---

# Services

Select one of the provided [services](https://core.hitchy.org/internals/components.html#services):

* [AuthenticationPassport](authentication-passport.md)
* [AuthenticationStrategies](authentication-strategies.md)
* [AuthManager](auth-manager.md)
* [Authorization](authorization.md)
* [AuthorizationTree](authorization-tree.md)
* [AuthorizationNode](authorization-node.md)
* [AuthorizationPolicyGenerator](authorization-policy-generator.md)
