---
prev: /api/service/
next: authentication-strategies.md
---

# AuthenticationPassport

This service integrates [Passport](https://www.passportjs.org/) with Hitchy's request routing. The service itself represents instance of Passport created for integration with Hitchy-based application.

This service is primarily for internal use. You should use it in edge cases to additionally customize Passport, only.

## Methods

### integrateWithHitchy()

**Signature:** `integrateWithHitchy(): void`

This method has been implemented for internal use. It is executed on every incoming request

* to set up handlers persisting authenticated user's information to server-side session and restoring it from there in follow-up requests and
* to instruct passport to [use](https://www.passportjs.org/docs/configure/#strategies) every configured strategy.
