---
prev: authorization-node.md
next: ../config.md
---

# AuthorizationPolicyGenerator

This service is generating functions suitable for [policy-handling](https://core.hitchy.org/internals/routing-basics.html#policies) requests. Generated functions filter mismatching requests thus help with writing more self-explanatory request routing configurations.

::: tip  
Users with administrator role always pass any generated policy.  
:::

## Methods

### hasRole()

**Signature:** `hasRole( roles ): Hitchy.Core.RequestPolicyHandler`

Generates request policy handler testing if a provided request's user is associated with one of the roles listed here. If request's user is associated with either role, the request processing is continued. Otherwise this policy responds with HTTP status code 403.

Argument `roles` is

* a string naming single role without any prefix or suffix or
* an array of such strings.

Returned function can be injected into runtime configuration for policies like this:

```javascript
module.exports = function() {
    const { hasRole } = this.service.AuthorizationPolicyGenerator;
    
    return {
        policies: {
            "GET /api/premium": hasRole( "customer" ),
            "GET /api/public": hasRole( [ "guest", "customer", "manager" ] ),
        },
    };
};
```

### mayAccess( resource )

**Signature:** `mayAccess( resource ): Hitchy.Core.RequestPolicyHandler`

Generates request policy handler testing if a provided request's user is authorized to access any of the listed resources.

Argument `resource` is

* a string naming single resource or
* an array of such resource names.

Resulting handler can be injected into runtime configuration for policies like this:

```javascript
module.exports = function() {
    const { mayAccess } = this.service.AuthorizationPolicyGenerator;
    
    return {
        policies: {
            "GET /api/premium": mayAccess( "plan.premium" ),
            "GET /api/export": mayAccess( [ "plan.premium", "level.manager" ] ),
        },
    };
};
```
