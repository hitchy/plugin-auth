---
prev: ../
next: false
---

# Models

Select one of the provided [models](https://core.hitchy.org/internals/components.html#models):

* [User](user.md)
* [Role](role.md)
* [UserToRole](user-to-role.md)
* [AuthorizationRule](authorization-rule.md)
