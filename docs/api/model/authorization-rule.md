---
prev: user-to-role.md
next: /api/service/
---

# AuthorizationRule

An AuthorizationRule is a [model](https://core.hitchy.org/internals/components.html#models) representing a single rule granting access on a [named resource](../../introduction.md#resources) to a particular [user](user.md) and/or a particular [role](role.md) or rejecting access on that resource from a user or role. It is an essential part of authorization management.

## Properties

### selector

This mandatory [string](https://odem.hitchy.org/guides/defining-models.html#strings) property selects a [resource](../../introduction.md#resources) (see the [example](../../introduction.md#an-example)) by its name.

### user

This optional [UUID](https://odem.hitchy.org/guides/defining-models.html#uuids) selects a single user which is granted access to selected resource.

::: warning  
A rule must select at least a user or a role. It may select both. Names are not supported here. This includes special name `*` for matching any user.  
:::

### role

This optional [UUID](https://odem.hitchy.org/guides/defining-models.html#uuids) selects a single role which is granted access to selected resource.

::: warning  
A rule must select at least a user or a role. It may select both. Names are not supported here. This includes special name `*` for matching any role.  
:::

### accept 

This [boolean](https://odem.hitchy.org/guides/defining-models.html#booleans) property indicates, whether rule is granting access to selected resource (`true`) or revoking it (`false`). The default value is `true`.

## Methods

AuthenticationRules do not have custom methods apart from [basic ones](https://odem.hitchy.org/api/model.html).
