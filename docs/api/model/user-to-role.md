---
prev: role.md
next: authorization-rule.md
---

# UserToRole

Every instance of this model links an instance of [User model](user.md) with a instance of [Role model](role.md).

This model is part of access control management at runtime.

## Properties

These are the properties provided per role:

### user

Provides [UUID](https://odem.hitchy.org/guides/defining-models.html#uuids) of user associated with a role.

### user

Provides [UUID](https://odem.hitchy.org/guides/defining-models.html#uuids) of role associated with a user.

## Methods

There no custom methods apart from [basic ones](https://odem.hitchy.org/api/model.html).
