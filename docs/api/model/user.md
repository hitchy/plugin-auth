---
prev: ../model/
next: role.md
---

# User

This model represents a single user in local database. It is meant to support access control management at runtime e.g. to have a user-customizable access control.

## Properties

These are the properties provided per user:

### name

The user's name is a required [string](https://odem.hitchy.org/guides/defining-models.html#strings) which is meant to be unique. It is used to pick a user for local authentication. Authorization rules are capable of addressing a user by name, too.

### password

This [string](https://odem.hitchy.org/guides/defining-models.html#strings) provides a salted one-way hash of user's password for local authentication or some other kind of token for use with a non-local strategy.

Passwords for use with local authentication are implicitly hashed with [SCRYPT](https://de.wikipedia.org/wiki/Scrypt).

### strategy

Every user can be associated with a particular passport strategy to use for authentication. This [string](https://odem.hitchy.org/guides/defining-models.html#strings) property is picking a strategy by its name. Default strategy as configured is used in case this property is empty or unset.

### strategyData

This [string](https://odem.hitchy.org/guides/defining-models.html#strings) property is meant to provide custom data specific to selected strategy. It's format is specific to that strategy. It might be used to pick a remote service confirming the user's authentication or represent additional meta data on it.

## Methods

### hashPassword()

**Signature:** `async hashPassword( cleartext, [ previous ] ): string`

This asynchronous method derives salted hash from provided clear-text password. The second argument is optional. When provided, the salt is extracted from it instead of generating a fresh random salt. This is required for comparing password hashes. And it should not be used for anything else. Using fresh salts whenever possible is essential to password security. 

### setPassword()

**Signature:** `async setPassword( password )`

Changes password of user as provided in clear text. This is a helper updating hashed password in local instance. You still need to [save the record](https://odem.hitchy.org/api/model.html#instance-save) to persist the change.

### verifyPassword()

**Signature:** `async verifyPassword( password ): boolean`

Compares this user's hashed password with a provided clear-text password by hashing the latter with the same salt extracted from the former and compare the hashes. The method is implicitly loading the record from persistent storage if required. It promises `true` if both hashes are identical and `false` otherwise.
