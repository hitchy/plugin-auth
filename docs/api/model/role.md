---
prev: user.md
next: user-to-role.md
---

# Role

This model represents a single _role_ in local database. A role can be applied to multiple users. Authorizations can be granted to or revoked from a role instead of a single user, thus affecting all users the role has been applied to.

It is part of access control management at runtime.

## Properties

These are the properties provided per role:

### name

For every role, this [string](https://odem.hitchy.org/guides/defining-models.html#strings) property is representing the role's unique name. It is used in authorization rules to grant access on a resource to a set of users or revoke it from them explicitly.

## Methods

Roles do not have custom methods apart from [basic ones](https://odem.hitchy.org/api/model.html).
