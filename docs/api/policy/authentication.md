---
prev: ../policy/
next: authorization.md
---

# AuthenticationPolicy

This class implements policy handlers for transparently processing requests for logging in and out by means of authenticating as a user relying on a [configured](../config.md#config-auth-strategies) [passport](https://www.passportjs.org/) [strategy](https://www.passportjs.org/packages/).

These are the provided handlers:

## initialize()

This policy handler is basically integrating passport with a request's handling. It is unconditionally injected into every incoming request by default.

The handler is adopting [passport's instructions for setting it up as a middleware](https://www.passportjs.org/docs/downloads/html/#middleware) to work in context of Hitchy framework.

## login()

Handles request for authentication via integrated passport's strategy as configured. 

This handler is essential for [default route supported to authenticate a user](../routing.md#post-api-auth-login-strategy).

::: tip Example  
There is an example for how to use this policy in [section on configuring custom strategies](../config.md#config-auth-strategies).
:::

## logout()

This handler is dropping any user currently authenticated in context of this request. 

It is essential for [default route supported to drop user authentication](../routing.md#get-api-auth-logout).

## mustBeAuthenticated()

This handler responds with HTTP status 403 in case there is no authenticated user in context of current request. Use this policy if you want to reject all requests to a URL prefix unless some user has authenticated. 

```json
{
    "policies": {
        "/api/protected": [ "authentication.mustBeAuthenticated" ]
    }
}
```
