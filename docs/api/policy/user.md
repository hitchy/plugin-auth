---
prev: authorization.md
next: ../model/
---

# UserPolicy

This class provides policy handlers helping with common tasks related to some authenticated user.

## mustBeMe()

This handler tests if current request URL contains parameter named `uuid` matching current user's UUID. The request is rejected with HTTP status 403 **on mismatch**.

## mustNotBeMe()

This handler tests if current request URL contains parameter named `uuid` matching current user's UUID. The request is rejected with HTTP status 403 **on match**.

## changePassword()

This request is transparently processing request for changing any currently authenticated user's password. It requires provision of authenticated user's current password and the desired new password in request body.

This policy handler is essential to [default route supporting change of a user's password](../routing.md#post-api-auth-password).