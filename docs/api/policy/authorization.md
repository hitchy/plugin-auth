---
prev: authentication.md
next: user.md
---

# AuthorizationPolicy

This class provides policy handlers helping with common authorization tasks.

## mustBeAdmin()

This handler responds with HTTP status 403 in case there is no current user or any authenticated user is not associated with the role configured to provide administrative privileges by means of granting access on all resources.

::: tip   
This policy handler can be combined with [authentication.mustBeAuthenticated](authentication.md#mustbeauthenticated) to deliver more useful response messages on requests completely lacking authentication.

```json
{
    "policies": {
        "/api/protected": [ 
            "authentication.mustBeAuthenticated", 
            "authorization.mustBeAdmin" 
        ]
    }
}
```
:::
