---
prev: ../
next: false
---

# Policies

Select one of the provided [policies](https://core.hitchy.org/internals/components.html#policies):

* [Authentication](authentication.md)
* [Authorization](authorization.md)
* [User](user.md)
