---
prev: /api/service/
next: routing.md
---

# Runtime Configuration

This plugin's runtime configuration is expected in section `auth` of your application's configuration. When complying with suggested filesystem layout for a Hitchy application, there should be a file **config/auth.js** in your application exposing this section similar to this:

```javascript
export const auth = {
    ...
};
```

The following parameters are supported there:

## config.auth.prefix

This optional string controls shared prefix of routes set up implicitly for providing basic authentication support. Its default is `/api/auth` resulting in routes `/api/auth/login`, `/api/auth/current` etc.

Implicit routing setup can be disabled on providing boolean `false` as prefix here.

## config.auth.admin

Provides name and/or password of admin user to create initially when no user
with administration privileges has been found in local database. This optional parameter
is an object consisting of properties 

* **role** selecting name of role granting full access to any associated user (default: `admin`),
* **name** selecting name of user to create on start of application if there is no user associated with that role (default: `admin`) and
* **password** providing password of that user to be created in clear text (default: `nimda`).

```javascript
export const auth = {
    admin: {
        name: "jane.doe",
        password: "my5ecr3t"
    },
};
```

:::tip  
Environment variables **HITCHY_ADMIN_NAME**, **HITCHY_ADMIN_PASSWORD** and **HITCHY_ADMIN_ROLE** can be used to override any configuration provided here to e.g. support container-driven setups.   
:::

## config.auth.roles

Lists role names to create on boot if missing in local database. This list is empty by default.

```javascript
export const auth = {
    roles: [ "guest", "customer", "manager" ],
};
```

## config.auth.authorizations

This section grants or revokes access on named resources to/from users and/or roles. These _authorizations_ are loaded on application start before reading custom rules from local database. 

:::tip  
Authorizations [read from local database](service/authorization-tree.md#loadfromdatabase) may replace authorizations given in runtime configuration.  
:::

It is an object-style hierarchy mapping resource names into names of users and/or roles access on selected resource is granted to or revoked from.

```javascript
export const auth = {
    authorizations: {
        "backup": "@managers",
        "backup.export": "-@noobs",
        "backup.import": "+@admins, -john.doe"
    },
};
```

The wildcard `*` can be used instead of a user's name to select _all users_. Accordingly, the wildcard `@*` can be used to select _all roles_.

```javascript
export const auth = {
    authorizations: {
        "backup": "*",
        "backup.export": "-@*, +john.doe",
        "backup.import": "+@admins, -john.doe"
    },
};
```

Lists of users and roles can be provided as strings using comma for separation as illustrated above. Actual array are supported, too:

```javascript
export const auth = {
    authorizations: {
        "backup": [ "@managers" ],
        "backup.export": [ "-@noobs" ],
        "backup.import": [ "+@admins", "-john.doe" ],
    },
};
```

Names of resources can be grouped by shared prefixes:

```javascript
export const auth = {
    authorizations: {
        backup: {
            people: [ "@managers" ],
            sub: {
                export: [ "-@noobs" ],
                import: [ "+@admins", "-john.doe" ],
            },
        }
    },
};
```

Resource names concatenate path segments separated by period from each other. Common prefixes can be stripped off when nesting authorizations as demonstrated before. Nesting requires provision of users and roles moved into separate property named `people`.

For improved readability, `people` list may be replaced with separate lists for `users` and `roles` as well as with lists `grant` and `revoke` grouping authorizations accordingly:

```javascript
export const auth = {
    authorizations: {
        backup: {
            grant: [ "@managers" ],
            sub: {
                export: {
                    revoke: [ "@noobs" ]
                },
                import: {
                    users: [ "-john.doe" ],
                    roles: [ "+admins" ]
                },
            },
        }
    },
};
```

## config.auth.strategies

This optional object maps unique names of supported authentication strategies into related implementations for use with [Passport](https://www.passportjs.org/).

```javascript
const { OAuthStrategy } = require( "passport-oauth" );

export const auth = {
    strategies: {
        oauth: new OAuthStrategy( options, localUserSelectorFn ),
    },
};
```

Any strategy listed here will be picked up on [integrating Passport with Hitchy's request routing](service/authentication-passport.md#integratewithhitchy). When implementing [policy-based](policy/authentication.md#login) authenticating requests, name of either strategy must be picked in request's parameter `strategy`:

```javascript
export default function() {
    const { login } = this.policy.Authentication;
    
    return {
        routes: {
            "GET /login/:strategy": login()
        }
    };
}
```

If request parameter is missing, [configured default strategy](#config-auth-defaultstrategy) is used.

Provide a custom local strategy in property `local` here to prevent built-in [local strategy](service/authentication-strategies.md#generatelocal) from being used. Omit this configuration to rely on that built-in local strategy for authenticating users against local database, only.

## config.auth.defaultStrategy

This optional string names strategy to use by default. Defaults to `local` itself. 

Any custom strategy named here must be [set up](#config-auth-strategies) properly.
