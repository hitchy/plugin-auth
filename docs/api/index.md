---
prev: ../guides/
next: false
---

# API Reference

## Components

This plugin exposes the following [components](https://core.hitchy.org/internals/components.html) in context of your application:

### Controllers

The following [controlllers](https://core.hitchy.org/internals/components.html#controllers) are provided for implementing some default endpoints:

* [User](controller/user.md)

### Policies

Several [policies](https://core.hitchy.org/internals/components.html#policies) are available for customizing authentication and authorization support in your application:

* [Authentication](policy/authentication.md)
* [Authorization](policy/authorization.md)
* [User](policy/user.md)

### Models

These [models](https://core.hitchy.org/internals/components.html#models) are provided to manage access control at runtime:

* [User](model/user.md)
* [Role](model/role.md)
* [UserToRole](model/user-to-role.md)
* [AuthorizationRule](model/authorization-rule.md)

### Services

Commonly useful [services](https://core.hitchy.org/internals/components.html#services) regarding authentication and authorization are:

* [AuthenticationPassport](service/authentication-passport.md)
* [AuthenticationStrategies](service/authentication-strategies.md)
* [AuthManager](service/auth-manager.md)
* [Authorization](service/authorization.md)
* [AuthorizationNode](service/authorization-node.md)
* [AuthorizationTree](service/authorization-tree.md)
* [AuthorizationPolicyGenerator](service/authorization-policy-generator.md)

## Configuration

In addition to components listed above the plugin is processing some optionally available [runtime configuration](config.md).

## Routing defaults

Unless [disabled in runtime configuration](config.md#config-auth-prefix), this plugin is always setting up [routes providing basic user authentication](routing.md) to a client.
