---
prev: /
next: /guides/
---

# Introduction

**@hitchy/plugin-auth** is a plugin for the server-side framework [Hitchy](https://core.hitchy.org). It provides request handlers and policies for server-side request [authentication](#authentication) and [authorization](#authorization). It processes [authorization rules](api/config.md#config-auth-authorizations) in [runtime configuration](https://core.hitchy.org/api/hitchy.html#configuration). There are [models](https://core.hitchy.org/internals/components.html#models) in a [document-oriented database](https://odem.hitchy.org) for managing [users](api/model/user.md), [roles](api/model/role) and their authorizations using [rules](api/model/authorization-rule.md) at runtime, too. 

## Authentication

Authentication refers to the process of assuring a user's identity.

This plugin includes request handlers for instantly supporting basic authentication against some local database managed with [Odem](https://odem.hitchy.org/).

::: tip Supported authentication strategies  
This plugin integrates with [Passport](http://www.passportjs.org/) to handle authentication. Passport provides a wide array of [strategies](http://www.passportjs.org/packages/) that can handle all kinds of authentication which might work with external sources, as well. There is a built-in strategy for local authentication used by default.  
:::

### Users

Requests may include data for authenticating as a user selected by its name. Usually, this involves additional provision of some identification token such as a password.

Server-side sessions keep track of authentication requests once they succeeded to consider follow-up requests of same client as authenticated, too. This relies on separate plugins [session](https://www.npmjs.com/package/@hitchy/plugin-session) and [cookies](https://www.npmjs.com/package/@hitchy/plugin-cookies). 

## Authorization

Authorization is the process of controlling an [identified user's](#authentication) permissions to access certain resources such as features, functions, data etc.

By intention, users have restricted access to your application. [Authorization rules](api/model/authorization-rule.md) associate a named resource with one or more [users](api/model/user.md) and/or [roles](api/model/role.md) for granting or revoking access on those resources.

### Administrator account

On every start of application, the plugin checks if there is a [configurable](api/config.md#config-auth-admin) [user](api/model/user.md) with full access to all routes and models and creates it if missing. Unless [configured](api/config.md#config-auth-admin) otherwise, that user's name is `admin` and its password is `nimda`.

### Resources

A resource is basically just a name, e.g. of a feature or some data entity in your application. It does not mean anything to this plugin on its own. In context of your application the name is meant to represent something you want to control access on.

Resource names work hierarchically by design. They consist of segments similar to a filesystem's pathname. However, segments of a resource name are separated by periods instead of slashes.

### Roles

Roles are an addition to managing users. There is a model for managing roles in a database. Every role is meant to represent a group of users. They simplify access control based on authorization rules.

### An example

Let's assume your application provides a set of dedicated features and you want to control who's permitted to use what feature. Every such feature is a _resource_. For example, your application could have a _print_ feature and an _export_ feature. The latter could be divided into different supported export formats such as _PDF_ and _Excel spreadsheet_.

On naming related resources, consider future improvements relying on resources not representing some feature, such as controlling access on individual data records of your application. Thus, you should start naming your resources with a type name such as `features`. Group features as good as possible and pick a second-level name for either group, e.g. `print` and `export`. If you've combined features in such a group, add another segment for either individual feature in that group, e.g. `pdf` and `excel` for the `export` group.

```
+- features
   +- print
   +- export
      +- pdf
      +- excel
```

In this case, resulting resource names are `features.print`, `features.export.pdf` and `features.export.excel`.

[Authorization rules](api/model/authorization-rule.md) are capable of granting/revoking access on either particular feature selected by its name as given above. In addition, authorization rules may address groups of features or all features altogether by using partial resource names such as `features` or `features.export`.

```json
{
    "features": "@platinum",
    "features.export": "@gold,@silver",
    "features.export.pdf": "-@silver,john.doe"
}
```

:::tip Example explained  
Users with `platinum` role are granted access to all features. Users with `gold` role may access all export features. Eventually, users with `silver` role may access all export features except for the PDF export. Without regards to being assigned to either role, user `john.doe` has been granted access on PDF export feature.  
:::

On checking a user's authorization to access some resource, you provide that resource's qualified resource name. All rules addressing this resource or any of its superordinated resources will be checked for granting/revoking access to/from current user and/or some role this user is associated with.

Your application's code can check a user's authorization using some service. In addition, a policy generator is included with this plugin to create separate policies to be injected into your application's routing configuration.
