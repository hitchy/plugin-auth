# @hitchy/plugin-auth

[![pipeline status](https://gitlab.com/hitchy/plugin-auth/badges/master/pipeline.svg)](https://gitlab.com/hitchy/plugin-auth/-/commits/master)

_request authentication and authorization for Hitchy framework_

## License

[MIT](LICENSE)

## Usage

Run this command in a hitchy-based project's root folder containing its **package.json** file:

```
npm i @hitchy/plugin-auth
```

It will install this plugin as a dependency.

After restarting your application the plugin is discovered by Hitchy automatically. It is injecting some routes instantly suitable for authentication based on a local database.

## Manual

Read the [plugin's official manual](https://auth.hitchy.org/) e.g. for [routes supported by default](https://auth.hitchy.org/api/routing.html) and how to use [all the components this plugin is introducing](https://auth.hitchy.org/api/).
