/* eslint-disable camelcase */

export default async function() {
	const { AuthenticationStrategies } = this.service;

	return {
		auth: {
			strategies: {
				saml: await AuthenticationStrategies.generateSaml( "saml", {
					// URL of IdP receiving SAML requests
					entryPoint: "https://idp.cepharum.de/auth/realms/hitchy-plugin-auth-ci-test/protocol/saml",
					// name of this client as registered with the IdP
					issuer: "ci-saml-test",
					// signature algorithm to use (set to prevent insecure SHA-1 used by default)
					signatureAlgorithm: "sha256",
					// URL for redirecting user to after successful login
					callbackUrl: "http://localhost:3000/api/auth/login/saml",
					// URL of local application's endpoint for logging out to be exposed as meta of this service provider
					logoutCallbackUrl: "http://localhost:3000/api/auth/logout",
					// public certificate of IdP's realm used for signing SAML responses
					cert: "MIICwzCCAasCBgF7fB5l4jANBgkqhkiG9w0BAQsFADAlMSMwIQYDVQQDDBpoaXRjaHktcGx1Z2luLWF1dGgtY2ktdGVzdDAeFw0yMTA4MjUwNzAxMTJaFw0zMTA4MjUwNzAyNTJaMCUxIzAhBgNVBAMMGmhpdGNoeS1wbHVnaW4tYXV0aC1jaS10ZXN0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAleKognu7fSeyl8fNss1KOj2dlbizrCoMEMa6vk6775rWdTD8lGwBUP5yJ0qGNPk9enJUOUiJVfK6/vX1D1TiiJq1q6IZ+wefF9RO+2rbn/itHKpis4Ha9K5vE6MmjB4h9mVOnsYq7CYaYFHaN5+qmzD5HLtnGT9IvM78Bmm8T3yTS04f2kT6gcQvkKpAwuJfthGuyk7s1E9L1SPNZ9nFy3wFntfESo9tlZVaRHGkUMXZurei9qKAoPHpi74/y8xaGGEjFHPtMo/UKYaO4PouRUMscy3Fl42h5uyZN81d78UC3rfhm5HRwn0R0i+2h9Az+nqfLk6JYqpbIPWKGIN1NQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQBfD1moiUDZBcZ+e1KPN0fOhKYFAK+gMrtyeqRkK+E64e3jcPeXYae/HDwqwzEu0sQ3Izre/eO8KaX5vixhdsaowuMuwwLdf5OSlL+i7ST6Du6BEu9661Ie2+jAIyv8yjPNtn8PQfQayPpA/CV1GmaUAQ/HIGgMv+UsaTcTZraebDxufO+I8LS2UAl9I9lBBtMJLDTZFnzf/IhJeh1ZJieZO3oQiyt5vdNw/kz/Azv/AG/8A7R4g5bX7soYo/UMNiTtkdExnlIIwDsa6RwfiOaAlz88oqg2nxhTosu0L0h+YlETPSDuuWWZTX6lfGh3gsPnlrNpCnOnM6ycT/PCDyad",
				} ),

				oidc: await AuthenticationStrategies.generateOpenIdConnect( "oidc", {
					// Provides URL of IdP for discovering settings for this
					// application's authentication realm.
					discovery_url: "https://idp.cepharum.de/auth/realms/hitchy-plugin-auth-ci-test",
					// Provides this application's name in context of discovered
					// IdP realm. Think of it as the application's username for
					// authenticating itself at the IdP.
					client_id: "ci-openid-test",
					// Provides IdP-specific secret used to authenticate this
					// application as a valid client of IdP. Think of it as the
					// application's password for authenticating itself.
					client_secret: "some-random-secret",
					// Lists valid URLs user may be redirected to after logging
					// in successfully at IdP.
					redirect_uris: ["http://localhost:3000/api/auth/login/oidc"],
					// Lists metadata/attributes of authenticated user to be
					// delivered by IdP in redirect after successful login.
					response_types: ["code"],
					// Lists valid URLs user may be redirected to after logging
					// out from IdP.
					post_logout_redirect_uris: ["http://localhost:3000/api/auth/logout/success"],
				} ),
			},

			authorizations: {
				"foo.public": "*",
				"foo.private": "tester",
			},
		}
	};
}
