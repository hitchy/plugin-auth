import { resolve } from "node:path";
import { createReadStream } from "node:fs";
import { fileURLToPath } from "node:url";

const __dirname = resolve( fileURLToPath( import.meta.url ), ".." );

export const routes = {
	"/": ( _, res ) => {
		createReadStream( resolve( __dirname, "../pages/index.html" ) )
			.pipe( res.type( "html" ) );
	},
	"/api/user": ( _, res ) => res.json( { success: true } ),
	"/api/auth/login/:strategy": ( _, res ) => {
		createReadStream( resolve( __dirname, "../pages/login.html" ) )
			.pipe( res.type( "html" ) );
	},
	"/api/auth/logout/success": ( _, res ) => {
		createReadStream( resolve( __dirname, "../pages/logout.html" ) )
			.pipe( res.type( "html" ) );
	},
};
