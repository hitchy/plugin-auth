import { describe, before, after, it } from "mocha";
import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

import "should";
import "should-http";

const Test = await SDT( Core );

describe( "Model User", () => {
	const ctx = {};

	before( Test.before( ctx, { plugin: true, projectFolder: false } ) );

	after( Test.after( ctx ) );


	it( "is available", () => {
		ctx.hitchy.api.models.User.should.be.ok();
	} );

	it( "can be constructed", () => {
		( () => new ctx.hitchy.api.models.User() ).should.not.throw();
	} );

	describe( "has method hashPassword which", () => {
		it( "exists", () => {
			const hashPassword = new ctx.hitchy.api.models.User().hashPassword;

			hashPassword.should.be.a.Function();
			hashPassword.should.have.length( 1 );
		} );

		it( "rejects if cleartext is missing", () => {
			return new ctx.hitchy.api.models.User().hashPassword().should.be.rejected();
		} );

		it( "can hash a password", async() => {
			const hashed = await new ctx.hitchy.api.models.User().hashPassword( "nimda" );

			/^{(?:SSHA512|SCRYPT)}[a-zA-Z0-9+/]{64,}={0,3}$/i.test( hashed ).should.be.true();
		} );

		it( "returns clearText if clearText is already hashed", async() => {
			const hashPassword = new ctx.hitchy.api.models.User().hashPassword;

			const a = await hashPassword( "nimda" );
			const b = await hashPassword( a );

			a.should.be.equal( b );
		} );

		it( "returns different hashed values for the same value", async() => {
			const hashPassword = new ctx.hitchy.api.models.User().hashPassword;

			const a = await hashPassword( "nimda" );
			const b = await hashPassword( "nimda" );

			a.should.not.be.equal( b );
		} );

		it( "can re-hash a password with the same salt used with another password", async() => {
			const hashPassword = new ctx.hitchy.api.models.User().hashPassword;

			const clearA = "nimda";
			const clearB = "NiMdA";

			const a = await hashPassword( clearA );

			a.should
				.be.equal( await hashPassword( clearA, a ) )
				.and.not.be.equal( await hashPassword( clearB, a ) )
				.and.not.be.equal( await hashPassword( clearA, await hashPassword( "whatever" ) ) );
		} );
	} );

	describe( "has method setPassword which", () => {
		it( "exists", () => {
			const setPassword = new ctx.hitchy.api.models.User().setPassword;

			setPassword.should.be.a.Function();
			setPassword.should.have.length( 1 );
		} );

		it( "saves the password hashed", async() => {
			const user = new ctx.hitchy.api.models.User();

			const password = await user.setPassword( "nimda" );

			user.password.should.be.equal( password );

			const hashed = await user.hashPassword( "nimda", password );

			user.password.should.be.equal( hashed );
		} );
	} );

	describe( "has method verifyPassword which", () => {
		it( "exists", () => {
			const verifyPassword = new ctx.hitchy.api.models.User().verifyPassword;

			verifyPassword.should.be.a.Function();
			verifyPassword.should.have.length( 1 );
		} );

		it( "can verify a password given as clearText", async() => {
			const user = new ctx.hitchy.api.models.User();

			const password = await user.setPassword( "nimda" );
			user.password.should.be.equal( password );

			( await user.verifyPassword( "nimda" ) ).should.be.true();
			( await user.verifyPassword( "Nimda" ) ).should.be.false();
			( await user.verifyPassword( "NiMdA" ) ).should.be.false();
		} );
	} );
} );
