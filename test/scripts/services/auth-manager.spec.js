import { describe, it, before, after } from "mocha";
import "should";

import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

const Test = await SDT( Core );

describe( "AuthManager service", () => {
	const ctx = {};

	before( Test.before( ctx, { plugin: true, projectFolder: false } ) );
	after( Test.after( ctx ) );

	describe( "exposes asUser() which", () => {
		it( "is a function", () => {
			ctx.hitchy.api.service.AuthManager.asUser.should.be.Function();
		} );

		it( "rejects name of a missing user by default", async() => {
			await ctx.hitchy.api.service.AuthManager.asUser( "baz" ).should.be.rejected();
		} );

		it( "accepts a name of a user, creates it if missing on demand and returns that user loaded from local database", async() => {
			const user = await ctx.hitchy.api.service.AuthManager.asUser( "foo", true );

			user.should.be.instanceOf( ctx.hitchy.api.models.User );
			user.name.should.be.equal( "foo" );
			( user.password == null ).should.be.true();

			( await new ctx.hitchy.api.models.User( user.$uuid ).$exists ).should.be.true();
		} );

		it( "accepts an instance of a user and returns it as provided", async() => {
			const foo = new ctx.hitchy.api.models.User();
			const user = await ctx.hitchy.api.service.AuthManager.asUser( foo );

			user.should.be.equal( foo );
		} );

		it( "rejects an instance of a role", async() => {
			const users = new ctx.hitchy.api.models.Role();

			await ctx.hitchy.api.service.AuthManager.asUser( users ).should.be.rejected();
		} );
	} );

	describe( "exposes asRole() which", () => {
		it( "is a function", () => {
			ctx.hitchy.api.service.AuthManager.asRole.should.be.Function();
		} );

		it( "rejects name of a missing role by default", async() => {
			await ctx.hitchy.api.service.AuthManager.asRole( "customers" ).should.be.rejected();
		} );

		it( "accepts a name of a role, creates it if missing on demand and returns that role loaded from local database", async() => {
			const role = await ctx.hitchy.api.service.AuthManager.asRole( "users", true );

			role.should.be.instanceOf( ctx.hitchy.api.models.Role );
			role.name.should.be.equal( "users" );

			( await new ctx.hitchy.api.models.Role( role.$uuid ).$exists ).should.be.true();
		} );

		it( "accepts an instance of a role and returns it as provided", async() => {
			const admins = new ctx.hitchy.api.models.Role();
			const user = await ctx.hitchy.api.service.AuthManager.asRole( admins );

			user.should.be.equal( admins );
		} );

		it( "rejects an instance of a user", async() => {
			const foo = new ctx.hitchy.api.models.User();

			await ctx.hitchy.api.service.AuthManager.asRole( foo ).should.be.rejected();
		} );
	} );

	describe( "exposes grantRoleToUser() which", () => {
		it( "is a function", () => {
			ctx.hitchy.api.service.AuthManager.grantRoleToUser.should.be.Function();
		} );

		it( "requires provision of two arguments", () => {
			ctx.hitchy.api.service.AuthManager.grantRoleToUser.should.have.length( 2 );
		} );

		it( "rejects names of missing user and role by default", async() => {
			await ctx.hitchy.api.service.AuthManager.grantRoleToUser( "customers", "baz" ).should.be.rejected();
		} );

		it( "accepts names of user and role, creating either if missing and grants selected role to selected user", async() => {
			await ctx.hitchy.api.service.AuthManager.grantRoleToUser( "friends", "jane", true );
		} );

		it( "succeeds on trying to grant same role to same user again", async() => {
			await ctx.hitchy.api.service.AuthManager.grantRoleToUser( "friends", "jane", true );
		} );

		it( "accepts instances of user and role provided instead of their names", async() => {
			const friends = await ctx.hitchy.api.service.AuthManager.asRole( "friends", true );
			const jane = await ctx.hitchy.api.service.AuthManager.asUser( "jane", true );

			await ctx.hitchy.api.service.AuthManager.grantRoleToUser( friends, jane );
		} );
	} );

	describe( "exposes revokeRoleFromUser() which", () => {
		it( "is a function", () => {
			ctx.hitchy.api.service.AuthManager.revokeRoleFromUser.should.be.Function();
		} );

		it( "requires provision of two arguments", () => {
			ctx.hitchy.api.service.AuthManager.revokeRoleFromUser.should.have.length( 2 );
		} );

		it( "rejects names of missing user and role by default", async() => {
			await ctx.hitchy.api.service.AuthManager.revokeRoleFromUser( "customers", "baz" ).should.be.rejected();
		} );

		it( "accepts names of user and role, creating either if missing and revokes selected role from selected user", async() => {
			await ctx.hitchy.api.service.AuthManager.revokeRoleFromUser( "friends", "jane", true );
		} );

		it( "succeeds on trying to revoke same role from same user again", async() => {
			await ctx.hitchy.api.service.AuthManager.revokeRoleFromUser( "friends", "jane", true );
		} );

		it( "accepts instances of user and role provided instead of their names", async() => {
			const friends = await ctx.hitchy.api.service.AuthManager.asRole( "friends", true );
			const jane = await ctx.hitchy.api.service.AuthManager.asUser( "jane", true );

			await ctx.hitchy.api.service.AuthManager.revokeRoleFromUser( friends, jane );
		} );
	} );

	describe( "exposes listUsersOfRole() which", () => {
		it( "is a function", () => {
			ctx.hitchy.api.service.AuthManager.listUsersOfRole.should.be.Function();
		} );

		it( "requires provision of a role", () => {
			ctx.hitchy.api.service.AuthManager.listUsersOfRole.should.have.length( 1 );
		} );

		it( "throws by default if selected role is missing", async() => {
			const { AuthManager } = ctx.hitchy.api.service;

			await ( await AuthManager.asRole( "customers", true ) ).remove();
			await AuthManager.listUsersOfRole( "customers" ).should.be.rejected();
		} );

		it( "creates selected role if missing on demand", async() => {
			const { AuthManager } = ctx.hitchy.api.service;

			await ( await AuthManager.asRole( "customers", true ) ).remove();
			( await AuthManager.listUsersOfRole( "customers", true ) ).should.be.an.Array().which.is.empty();
		} );

		it( "accepts name of missing role, creates it on demand and returns empty list of associated users", async() => {
			const { AuthManager } = ctx.hitchy.api.service;

			await ( await AuthManager.asRole( "customers", true ) ).remove();
			( await AuthManager.listUsersOfRole( "customers", true ) ).should.be.an.Array().which.is.empty();
		} );

		it( "lists users associated with named role by default", async() => {
			const { AuthManager } = ctx.hitchy.api.service;

			await ( await AuthManager.asRole( "customers", true ) ).remove();
			await AuthManager.grantRoleToUser( "customers", "jane", true );
			await AuthManager.grantRoleToUser( "customers", "peter", true );

			const list = await AuthManager.listUsersOfRole( "customers" );

			list.should.be.an.Array().which.has.length( 2 );
			list.map( user => user.name ).should.be.containDeep( [ "jane", "peter" ] );
		} );

		it( "lists UUIDs of users associated with named role, only, on demand", async() => {
			const { User } = ctx.hitchy.api.model;
			const { AuthManager } = ctx.hitchy.api.service;

			await ( await AuthManager.asRole( "customers", true ) ).remove();
			await AuthManager.grantRoleToUser( "customers", "jane", true );
			await AuthManager.grantRoleToUser( "customers", "peter", true );

			const list = await AuthManager.listUsersOfRole( "customers", false, true );

			list.should.be.an.Array().which.has.length( 2 );
			list.map( user => user.name ).should.be.deepEqual( [ undefined, undefined ] );
			list.map( user => user instanceof User ).should.be.deepEqual( [ true, true ] );
		} );

		it( "does not list users which have been revoked from role", async() => {
			const { AuthManager } = ctx.hitchy.api.service;

			await ( await AuthManager.asRole( "customers", true ) ).remove();
			await AuthManager.grantRoleToUser( "customers", "jane", true );
			await AuthManager.grantRoleToUser( "customers", "peter", true );
			await AuthManager.revokeRoleFromUser( "customers", "peter" );

			const list = await AuthManager.listUsersOfRole( "customers" );

			list.should.be.an.Array().which.has.length( 1 );
			list.map( user => user.name ).should.be.containDeep( ["jane"] );
		} );
	} );

	describe( "exposes createAdminUserIfMissing() which", () => {
		it( "is a function", () => {
			ctx.hitchy.api.service.AuthManager.createAdminUserIfMissing.should.be.Function();
		} );

		it( "does not require provision of any argument", () => {
			ctx.hitchy.api.service.AuthManager.createAdminUserIfMissing.should.have.length( 0 );
		} );

		it( "returns list containing the created admin user if it was missing before", async() => {
			const { AuthManager } = ctx.hitchy.api.service;

			await ( await AuthManager.asUser( "admin" ) ).remove();

			const admins = await AuthManager.createAdminUserIfMissing();
			admins.should.be.an.Array().which.has.length( 1 );
			admins[0].name.should.be.equal( "admin" );
		} );

		it( "reassigns existing admin user to admin role", async() => {
			const { AuthManager } = ctx.hitchy.api.service;

			await ( await AuthManager.asUser( "admin" ) ).remove();
			await ( await AuthManager.asRole( "admin" ) ).remove();

			const user = await AuthManager.asUser( "admin", true );
			let roles = await AuthManager.listRolesOfUser( user );
			roles.should.be.an.Array().which.is.empty();

			const admins = await AuthManager.createAdminUserIfMissing();
			admins.should.be.an.Array().which.has.length( 1 );
			admins[0].name.should.be.equal( "admin" );
			admins[0].$uuid.equals( user.$uuid ).should.be.true();

			roles = await AuthManager.listRolesOfUser( admins[0] );
			roles.should.be.an.Array().which.is.has.length( 1 );
		} );

		it( "returns list containing all existing admin users", async() => {
			const { AuthManager } = ctx.hitchy.api.service;

			const admin1 = await AuthManager.asUser( "admin1", true );
			const admin2 = await AuthManager.asUser( "admin2", true );

			await AuthManager.grantRoleToUser( "admin", admin1 );
			await AuthManager.grantRoleToUser( "admin", admin2 );

			const admins = await AuthManager.createAdminUserIfMissing();
			admins.should.be.an.Array();
			admins.length.should.be.greaterThanOrEqual( 2 );
			admins.map( user => user.name ).should.containDeep( [ "admin1", "admin2" ] );
		} );
	} );
} );
