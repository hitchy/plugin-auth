import { describe, it, beforeEach, afterEach } from "mocha";
import "should";

import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

const Test = await SDT( Core );

describe( "Authorization service", () => {
	const ctx = {};

	afterEach( Test.after( ctx ) );
	beforeEach( Test.before( ctx, {
		plugin: true,
	} ) );

	describe( "provides method `mayAccess()` which", () => {
		it( "exists", () => {
			ctx.hitchy.api.service.Authorization.mayAccess.should.be.a.Function();
		} );

		it( "promises `false` when checking access on explicitly private resource without a user", async() => {
			await ctx.hitchy.api.service.Authorization.mayAccess( undefined, "foo.private" ).should.be.resolvedWith( false );
		} );

		it( "promises `false` when checking access on implicitly private resource without a user", async() => {
			await ctx.hitchy.api.service.Authorization.mayAccess( undefined, "foo.not-defined" ).should.be.resolvedWith( false );
		} );

		it( "promises `true` when checking access on explicitly public resource without a user", async() => {
			await ctx.hitchy.api.service.Authorization.mayAccess( undefined, "foo.public" ).should.be.resolvedWith( true );
		} );

		// ----------------------------------------------------------

		it( "promises `false` when checking access on explicitly private resource with an existing user access has not been granted to", async() => {
			const johnDoe = await ctx.hitchy.api.service.AuthManager.asUser( "john.doe", true );

			await ctx.hitchy.api.service.Authorization.mayAccess( johnDoe, "foo.private" ).should.be.resolvedWith( false );
			await ctx.hitchy.api.service.Authorization.mayAccess( "john.doe", "foo.private" ).should.be.resolvedWith( false );
		} );

		it( "promises `false` when checking access on implicitly private resource with an existing user", async() => {
			const johnDoe = await ctx.hitchy.api.service.AuthManager.asUser( "john.doe", true );

			await ctx.hitchy.api.service.Authorization.mayAccess( johnDoe, "foo.not-defined" ).should.be.resolvedWith( false );
			await ctx.hitchy.api.service.Authorization.mayAccess( "john.doe", "foo.not-defined" ).should.be.resolvedWith( false );
		} );

		it( "promises `true` when checking access on explicitly public resource with an existing user", async() => {
			const johnDoe = await ctx.hitchy.api.service.AuthManager.asUser( "john.doe", true );

			await ctx.hitchy.api.service.Authorization.mayAccess( johnDoe, "foo.public" ).should.be.resolvedWith( true );
			await ctx.hitchy.api.service.Authorization.mayAccess( "john.doe", "foo.public" ).should.be.resolvedWith( true );
		} );

		// ----------------------------------------------------------

		it( "promises `false` when checking access on explicitly private resource with an unknown user access has not been granted to", async() => {
			await ctx.hitchy.api.service.Authorization.mayAccess( "jane.doe", "foo.private" ).should.be.resolvedWith( false );
		} );

		it( "promises `false` when checking access on explicitly private resource with an unknown user access has been granted to", async() => {
			await ctx.hitchy.api.service.Authorization.mayAccess( "tester", "foo.private" ).should.be.resolvedWith( false );
		} );

		it( "promises `false` when checking access on implicitly private resource with an unknown user", async() => {
			await ctx.hitchy.api.service.Authorization.mayAccess( "jane.doe", "foo.not-defined" ).should.be.resolvedWith( false );
		} );

		it( "promises `true` when checking access on explicitly public resource with an unknown user", async() => {
			await ctx.hitchy.api.service.Authorization.mayAccess( "jane.doe", "foo.public" ).should.be.resolvedWith( true );
		} );

		// ----------------------------------------------------------

		it( "promises `true` when checking access on explicitly private resource with an existing user access has been granted to", async() => {
			const tester = await ctx.hitchy.api.service.AuthManager.asUser( "tester", true );

			await ctx.hitchy.api.service.Authorization.mayAccess( tester, "foo.private" ).should.be.resolvedWith( true );
			await ctx.hitchy.api.service.Authorization.mayAccess( "tester", "foo.private" ).should.be.resolvedWith( true );
		} );

		it( "promises `false` when checking access on implicitly private resource with an existing user", async() => {
			const tester = await ctx.hitchy.api.service.AuthManager.asUser( "tester", true );

			await ctx.hitchy.api.service.Authorization.mayAccess( tester, "foo.not-defined" ).should.be.resolvedWith( false );
			await ctx.hitchy.api.service.Authorization.mayAccess( "tester", "foo.not-defined" ).should.be.resolvedWith( false );
		} );

		it( "promises `true` when checking access on explicitly public resource with an existing user", async() => {
			const tester = await ctx.hitchy.api.service.AuthManager.asUser( "tester", true );

			await ctx.hitchy.api.service.Authorization.mayAccess( tester, "foo.public" ).should.be.resolvedWith( true );
			await ctx.hitchy.api.service.Authorization.mayAccess( "tester", "foo.public" ).should.be.resolvedWith( true );
		} );
	} );
} );
