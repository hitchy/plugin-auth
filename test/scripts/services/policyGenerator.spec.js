import { describe, before, after, it } from "mocha";
import "should";
import "should-http";

import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

import { getSID } from "../../helper.js";

const Test = await SDT( Core );

describe( "AuthorizationPolicyGenerator", () => {
	describe( "is a service which", () => {
		const ctx = {};

		before( Test.before( ctx, { plugin: true, projectFolder: false } ) );
		after( Test.after( ctx ) );

		it( "is available", () => {
			ctx.hitchy.api.services.should.have.property( "AuthorizationPolicyGenerator" );
		} );

		it( "provides generators hasRole() and mayAccess()", () => {
			const { AuthorizationPolicyGenerator } = ctx.hitchy.api.services;

			AuthorizationPolicyGenerator.should.have.property( "hasRole" ).which.is.a.Function();
			AuthorizationPolicyGenerator.should.have.property( "mayAccess" ).which.is.a.Function();
		} );
	} );

	describe( "exposes hasRole() generating a policy handler which", () => {
		const ctx = {};
		let sid;

		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"config/all.cjs": `
					module.exports = function () {
						return {
							policies: {
								"/developer": this.services.AuthorizationPolicyGenerator.hasRole( "developer" )
							},
							routes: {
								"/developer": ( _, res ) => res.json( { success: true } ),
							},
						};
					}
				`,
			}
		} ) );

		before( async() => {
			const am = ctx.hitchy.api.service.AuthManager;

			const dev = await am.asUser( "developer", true );
			const devRole = await am.asRole( "developer", true );

			const nonDev = await am.asUser( "nonDeveloper", true );
			const nonDevRole = await am.asRole( "nonDeveloper", true );

			await am.grantRoleToUser( devRole, dev );
			await am.grantRoleToUser( nonDevRole, nonDev );
			await dev.setPassword( "developer" );
			await nonDev.setPassword( "nonDeveloper" );
			await dev.save();
			await nonDev.save();
		} );

		after( async() => {
			const am = ctx.hitchy.api.service.AuthManager;

			await Promise.all( [
				( await am.asUser( "developer" ) ).remove(),
				( await am.asUser( "nonDeveloper" ) ).remove(),
				( await am.asRole( "developer" ) ).remove(),
				( await am.asRole( "nonDeveloper" ) ).remove(),
			] );
		} );

		after( Test.after( ctx ) );

		it( "rejects unauthenticated requests on protected route which is starting new session", async() => {
			const res = await ctx.get( "/developer" );

			res.should.have.status( 403 );
			res.headers.should.have.property( "set-cookie" ).which.is.an.Array().which.is.not.empty();

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "rejects unauthenticated requests on protected route on follow-up request in same session", async() => {
			const res = await ctx.get( "/developer", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );
		} );

		it( "allows authentication as user `developer`", async() => {
			const res = await ctx.post( "/api/auth/login", "username=developer&password=developer", {
				cookie: `sessionId=${sid}`,
				"Content-Type": "application/x-www-form-urlencoded",
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );
			res.data.success.should.be.true();

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "allows authenticated request on protected route as user with required role", async() => {
			const res = await ctx.get( "/developer", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 200 );
			res.headers.should.not.have.property( "set-cookie" );
			res.data.success.should.be.true();
		} );

		it( "allows request for logging off user `developer`", async() => {
			const res = await ctx.get( "/api/auth/logout", {
				cookie: `sessionId=${sid}`,
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "rejects unauthenticated request on protected route after user `developer` has logged out", async() => {
			const res = await ctx.get( "/developer", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );
		} );

		it( "allows authentication as user `nonDeveloper`", async() => {
			const res = await ctx.post( "/api/auth/login", "username=nonDeveloper&password=nonDeveloper", {
				cookie: `sessionId=${sid}`,
				"Content-Type": "application/x-www-form-urlencoded",
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );
			res.data.success.should.be.true();

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "rejects authenticated request on protected route as user without required role", async() => {
			const res = await ctx.get( "/developer", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );
		} );

		it( "allows request for logging off user `nonDeveloper`", async() => {
			const res = await ctx.get( "/api/auth/logout", {
				cookie: `sessionId=${sid}`,
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "rejects unauthenticated request on protected route after user `nonDeveloper` has logged out", async() => {
			const res = await ctx.get( "/developer", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );
		} );

		it( "allows authentication as user `admin`", async() => {
			const res = await ctx.post( "/api/auth/login", "username=admin&password=nimda", {
				cookie: `sessionId=${sid}`,
				"Content-Type": "application/x-www-form-urlencoded",
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );
			res.data.success.should.be.true();

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "allows authenticated request on protected route as user `admin`", async() => {
			const res = await ctx.get( "/developer", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 200 );
			res.headers.should.not.have.property( "set-cookie" );
			res.data.success.should.be.true();
		} );

		it( "allows request for logging off user `admin`", async() => {
			const res = await ctx.get( "/api/auth/logout", {
				cookie: `sessionId=${sid}`,
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "rejects unauthenticated request on protected route after user `admin` has logged out", async() => {
			const res = await ctx.get( "/developer", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );
		} );
	} );

	describe( "exposes mayAccess() generating a policy handler which", () => {
		const ctx = {};
		let sid;

		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"config/all.cjs": `
					"use strict";
					
					module.exports = function () {
						const { AuthorizationPolicyGenerator } = this.services;
						
						return {
							auth: {
								authorizations: {
									"developer.read": "@developer",
								},
							},
							policies: {
								"/exact": AuthorizationPolicyGenerator.mayAccess( "developer.read" ),
								"/sub": AuthorizationPolicyGenerator.mayAccess( "developer.read.overview" ),
							},
							routes: {
								"/exact": ( _, res ) => res.json( { success: true } ),
								"/sub": ( _, res ) => res.json( { success: true } ),
							},
						};
					};
				`,
			},
		} ) );

		before( async() => {
			const am = ctx.hitchy.api.service.AuthManager;

			const dev = await am.asUser( "developer", true );
			const devRole = await am.asRole( "developer", true );

			const nonDev = await am.asUser( "nonDeveloper", true );
			const nonDevRole = await am.asRole( "nonDeveloper", true );

			await am.grantRoleToUser( devRole, dev );
			await am.grantRoleToUser( nonDevRole, nonDev );

			await dev.setPassword( "developer" );
			await dev.save();

			await nonDev.setPassword( "nonDeveloper" );
			await nonDev.save();
		} );

		after( async() => {
			const am = ctx.hitchy.api.service.AuthManager;

			await Promise.all( [
				( await am.asUser( "developer" ) ).remove(),
				( await am.asUser( "nonDeveloper" ) ).remove(),
				( await am.asRole( "developer" ) ).remove(),
				( await am.asRole( "nonDeveloper" ) ).remove(),
			] );
		} );

		after( Test.after( ctx ) );

		it( "rejects unauthenticated request on protected route which is starting new session", async() => {
			let res = await ctx.get( "/exact" );

			res.should.have.status( 403 );
			res.headers.should.have.property( "set-cookie" ).which.is.an.Array().which.is.not.empty();

			res = await ctx.get( "/sub" );

			res.should.have.status( 403 );
			res.headers.should.have.property( "set-cookie" ).which.is.an.Array().which.is.not.empty();

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "rejects unauthenticated request on protected route on follow-up request in same session", async() => {
			let res = await ctx.get( "/exact", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );

			res = await ctx.get( "/sub", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );
		} );

		it( "allows authentication as user `developer`", async() => {
			const res = await ctx.post( "/api/auth/login", "username=developer&password=developer", {
				cookie: `sessionId=${sid}`,
				"Content-Type": "application/x-www-form-urlencoded",
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );
			res.data.success.should.be.true();

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "allows authenticated request on protected route as user with required role", async() => {
			let res = await ctx.get( "/exact", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 200 );
			res.headers.should.not.have.property( "set-cookie" );
			res.data.success.should.be.true();

			res = await ctx.get( "/sub", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 200 );
			res.headers.should.not.have.property( "set-cookie" );
			res.data.success.should.be.true();
		} );

		it( "allows request for logging off user `developer`", async() => {
			const res = await ctx.get( "/api/auth/logout", {
				cookie: `sessionId=${sid}`,
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "rejects unauthenticated request on protected route after user `developer` has logged out", async() => {
			let res = await ctx.get( "/exact", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );

			res = await ctx.get( "/sub", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );
		} );

		it( "allows authentication as user `nonDeveloper`", async() => {
			const res = await ctx.post( "/api/auth/login", "username=nonDeveloper&password=nonDeveloper", {
				cookie: `sessionId=${sid}`,
				"Content-Type": "application/x-www-form-urlencoded",
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );
			res.data.success.should.be.true();

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "rejects authenticated request on protected route as user without required role", async() => {
			let res = await ctx.get( "/exact", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );

			res = await ctx.get( "/sub", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );
		} );

		it( "allows request for logging off user `nonDeveloper`", async() => {
			const res = await ctx.get( "/api/auth/logout", {
				cookie: `sessionId=${sid}`,
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "rejects unauthenticated request on protected route after user `nonDeveloper` has logged out", async() => {
			let res = await ctx.get( "/exact", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );

			res = await ctx.get( "/sub", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );
		} );

		it( "allows authentication as user `admin`", async() => {
			const res = await ctx.post( "/api/auth/login", "username=admin&password=nimda", {
				cookie: `sessionId=${sid}`,
				"Content-Type": "application/x-www-form-urlencoded",
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );
			res.data.success.should.be.true();

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "allows authenticated request on protected route as user `admin`", async() => {
			let res = await ctx.get( "/exact", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 200 );
			res.headers.should.not.have.property( "set-cookie" );
			res.data.success.should.be.true();

			res = await ctx.get( "/sub", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 200 );
			res.headers.should.not.have.property( "set-cookie" );
			res.data.success.should.be.true();
		} );

		it( "allows request for logging off user `admin`", async() => {
			const res = await ctx.get( "/api/auth/logout", {
				cookie: `sessionId=${sid}`,
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "set-cookie" );

			sid = getSID( res.headers );
			sid.should.be.ok();
		} );

		it( "rejects unauthenticated request on protected route after user `admin` has logged out", async() => {
			let res = await ctx.get( "/exact", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );

			res = await ctx.get( "/sub", {
				cookie: `sessionId=${sid}`
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "set-cookie" );
		} );
	} );
} );
