import { describe, it, before, after, beforeEach } from "mocha";
import "should";

import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

const Test = await SDT( Core );

describe( "AuthorizationTree service", () => {
	const ctx = {};

	before( Test.before( ctx, { plugin: true, projectFolder: false } ) );
	after( Test.after( ctx ) );

	beforeEach( () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		current.clear();

		current.node.children.should.be.an.Object().which.is.empty();
		current.node.users.accept.should.be.an.Object().which.is.empty();
		current.node.users.reject.should.be.an.Object().which.is.empty();
		current.node.roles.accept.should.be.an.Object().which.is.empty();
		current.node.roles.reject.should.be.an.Object().which.is.empty();
	} );

	describe( "exposes a singleton instance as `current` which", () => {
		it( "exists", () => {
			ctx.hitchy.api.service.AuthorizationTree.current.should.be.ok();
		} );

		it( "provides method `addRule()` for adding an authorization rule", () => {
			ctx.hitchy.api.service.AuthorizationTree.current.addRule.should.be.a.Function();
		} );

		it( "provides method `removeRule()` for removing an authorization rule", () => {
			ctx.hitchy.api.service.AuthorizationTree.current.removeRule.should.be.a.Function();
		} );

		it( "provides method `isAuthorized()` for testing whether some user or role is authorized to access a selected resource due to added rules", () => {
			ctx.hitchy.api.service.AuthorizationTree.current.isAuthorized.should.be.a.Function();
		} );

		it( "provides method `selectNode()` to look up node of tree representing some selected resource access may be granted or revoked", () => {
			ctx.hitchy.api.service.AuthorizationTree.current.selectNode.should.be.a.Function();
		} );

		it( "provides method `loadFromConfiguration()` to add rules found in configuration", () => {
			ctx.hitchy.api.service.AuthorizationTree.current.loadFromConfiguration.should.be.a.Function();
		} );
	} );

	it( "supports creating tree by looking up nodes", () => {
		const { AuthorizationTree, AuthorizationNode } = ctx.hitchy.api.service;

		( AuthorizationTree.current.selectNode( "foo.bar", false ) == null ).should.be.true();
		AuthorizationTree.current.selectNode( "foo.bar" ).should.be.instanceOf( AuthorizationNode );
		( AuthorizationTree.current.selectNode( "foo.bar", false ) == null ).should.be.false();
		AuthorizationTree.current.selectNode( "foo.bar", false ).should.be.instanceOf( AuthorizationNode );
	} );

	it( "creates separate nodes per segment of selected resource's path name", () => {
		const { AuthorizationTree, AuthorizationNode } = ctx.hitchy.api.service;

		const baz = AuthorizationTree.current.selectNode( "bar.foo.baz" );
		const foo = AuthorizationTree.current.selectNode( "bar.foo", false );
		const bar = AuthorizationTree.current.selectNode( "bar", false );

		baz.should.be.instanceOf( AuthorizationNode );
		foo.should.be.instanceOf( AuthorizationNode );
		bar.should.be.instanceOf( AuthorizationNode );

		baz.should.not.be.equal( foo );
		bar.should.not.be.equal( foo );
		bar.should.not.be.equal( baz );
	} );

	it( "accepts authorization rules being added/removed for granting/revoking access to/from user and/or role", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.true();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();

		current.addRule( {
			selector: "feature.export.pdf",
			user: "john.doe",
			accept: true,
		} );

		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.true();

		current.removeRule( {
			selector: "feature.export.pdf",
			user: "john.doe",
			accept: true,
		} );

		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
	} );

	it( "accepts authorization rules being added/removed for granting access to everyone using *", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.true();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();

		current.addRule( {
			selector: "feature.export.pdf",
			user: "*",
			accept: true,
		} );

		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.true();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.true();

		current.removeRule( {
			selector: "feature.export.pdf",
			user: "*",
			accept: true,
		} );

		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();
	} );

	it( "accepts authorization rules being added/removed for granting access to every role using *", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.true();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();

		current.addRule( {
			selector: "feature.export.pdf",
			role: "*",
			accept: true,
		} );

		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.true();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.true();

		current.removeRule( {
			selector: "feature.export.pdf",
			role: "*",
			accept: true,
		} );

		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();
	} );

	it( "accepts combination of * used as user on a parent node with more specific rules opposing that parent node", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		( current.selectNode( "feature", false ) == null ).should.be.true();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.true();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf" ).should.be.false();

		current.addRule( {
			selector: "feature",
			user: "*",
			accept: true,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.true();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.true();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.true();
		current.isAuthorized( "feature.export.pdf" ).should.be.true();

		current.addRule( {
			selector: "feature.export.pdf",
			user: "*",
			accept: false,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf" ).should.be.false();

		current.removeRule( {
			selector: "feature.export.pdf",
			user: "*",
			accept: false,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.true();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.true();
		current.isAuthorized( "feature.export.pdf" ).should.be.true();

		current.removeRule( {
			selector: "feature",
			user: "*",
			accept: true,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf" ).should.be.false();
	} );

	it( "accepts combination of * used as role on a parent node with more specific rules opposing that parent node", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		( current.selectNode( "feature", false ) == null ).should.be.true();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.true();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();

		current.addRule( {
			selector: "feature",
			role: "*",
			accept: true,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.true();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.true();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.true();

		current.addRule( {
			selector: "feature.export.pdf",
			role: "*",
			accept: false,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();

		current.removeRule( {
			selector: "feature.export.pdf",
			role: "*",
			accept: false,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.true();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.true();

		current.removeRule( {
			selector: "feature",
			role: "*",
			accept: true,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();
	} );

	it( "accepts combination of * used as user and role on a parent node with more specific rules opposing that parent node", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		( current.selectNode( "feature", false ) == null ).should.be.true();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.true();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();

		current.addRule( {
			selector: "feature",
			user: "*",
			accept: true,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.true();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.true();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.true();

		current.addRule( {
			selector: "feature.export.pdf",
			role: "*",
			accept: false,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();

		current.removeRule( {
			selector: "feature.export.pdf",
			role: "*",
			accept: false,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.true();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.true();

		current.removeRule( {
			selector: "feature",
			user: "*",
			accept: true,
		} );

		( current.selectNode( "feature", false ) == null ).should.be.false();
		( current.selectNode( "feature.export.pdf", false ) == null ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();
	} );

	it( "accepts combination of * and actual names used as user and role on a hierarchy of nodes with more specific rules opposing more generic ones", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "jane.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe", "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "jane.doe", "users" ).should.be.false();

		current.isAuthorized( "feature.export.pdf.sub", "john.doe", "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf.sub", "jane.doe", "users" ).should.be.false();

		current.isAuthorized( "feature.export", "john.doe", "users" ).should.be.false();
		current.isAuthorized( "feature.export", "jane.doe", "users" ).should.be.false();

		current.isAuthorized( "feature", "john.doe", "users" ).should.be.false();
		current.isAuthorized( "feature", "jane.doe", "users" ).should.be.false();

		current.addRule( {
			selector: "feature",
			user: "*",
			accept: true,
		} );

		current.addRule( {
			selector: "feature.export",
			role: "*",
			accept: false,
		} );

		current.addRule( {
			selector: "feature.export",
			user: "john.doe",
			accept: true,
		} );

		current.addRule( {
			selector: "feature.export.pdf",
			role: "users",
			accept: false,
		} );

		current.addRule( {
			selector: "feature.export.pdf",
			user: "jane.doe",
			accept: true,
		} );

		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.true();
		current.isAuthorized( "feature.export.pdf", undefined, "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "jane.doe" ).should.be.true();
		current.isAuthorized( "feature.export.pdf", "john.doe", "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "jane.doe", "users" ).should.be.true();

		current.isAuthorized( "feature.export.pdf.sub", "john.doe", "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf.sub", "jane.doe", "users" ).should.be.true();

		current.isAuthorized( "feature.export", "john.doe", "users" ).should.be.true();
		current.isAuthorized( "feature.export", "jane.doe", "users" ).should.be.false();

		current.isAuthorized( "feature", "john.doe", "users" ).should.be.true();
		current.isAuthorized( "feature", "jane.doe", "users" ).should.be.true();
	} );

	it( "rejects on invalid combination of * granting and revoking to all users on the same node explicitly", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();

		current.addRule( {
			selector: "feature.export",
			user: "*",
			accept: false,
		} );

		current.addRule( {
			selector: "feature.export",
			user: "*",
			accept: true,
		} );

		current.isAuthorized( "feature.export", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
	} );

	it( "rejects on invalid combination of * granting and revoking to all roles on the same node explicitly", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();

		current.addRule( {
			selector: "feature.export",
			role: "*",
			accept: false,
		} );

		current.addRule( {
			selector: "feature.export",
			role: "*",
			accept: true,
		} );

		current.isAuthorized( "feature.export", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export", "john.doe", "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe", "users" ).should.be.false();
	} );

	it( "rejects on invalid combination of granting and revoking to the same user on the same node explicitly", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();

		current.addRule( {
			selector: "feature.export",
			user: "john.doe",
			accept: false,
		} );

		current.addRule( {
			selector: "feature.export",
			user: "john.doe",
			accept: true,
		} );

		current.isAuthorized( "feature.export", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
	} );

	it( "rejects on invalid combination of granting and revoking to the same role on the same node explicitly", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();

		current.addRule( {
			selector: "feature.export",
			role: "users",
			accept: false,
		} );

		current.addRule( {
			selector: "feature.export",
			role: "users",
			accept: true,
		} );

		current.isAuthorized( "feature.export", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe" ).should.be.false();
		current.isAuthorized( "feature.export", "john.doe", "users" ).should.be.false();
		current.isAuthorized( "feature.export.pdf", "john.doe", "users" ).should.be.false();
	} );

	it( "provides garbage collection for dropping sparse nodes of tree", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		current.selectNode( "premium.import.xls" );

		( current.selectNode( "premium.import.xls", false ) == null ).should.be.false();
		( current.selectNode( "premium.import", false ) == null ).should.be.false();
		( current.selectNode( "premium", false ) == null ).should.be.false();

		current._gcCounter = -1; // required to enforce garbage collection in next line
		current.gc();

		( current.selectNode( "premium.import.xls", false ) == null ).should.be.true();
		( current.selectNode( "premium.import", false ) == null ).should.be.true();
		( current.selectNode( "premium", false ) == null ).should.be.true();

		current.selectNode( "premium.import.xls" );

		current.addRule( {
			selector: "premium.import",
			user: "john.doe",
			accept: true,
		} );

		current._gcCounter = -1; // required to enforce garbage collection in next line
		current.gc();

		( current.selectNode( "premium.import.xls", false ) == null ).should.be.true();
		( current.selectNode( "premium.import", false ) == null ).should.be.false();
		( current.selectNode( "premium", false ) == null ).should.be.false();
	} );

	it( "ignores default as soon as there is a matching rule", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		current.isAuthorized( "feature.backup.remote.s3", undefined, undefined ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", undefined ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", [] ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", undefined, "managers" ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", [], "managers" ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", "managers" ).should.be.false();

		current.isAuthorized( "feature.backup.remote.s3", undefined, undefined, true ).should.be.true();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", undefined, true ).should.be.true();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", [], true ).should.be.true();
		current.isAuthorized( "feature.backup.remote.s3", undefined, "managers", true ).should.be.true();
		current.isAuthorized( "feature.backup.remote.s3", [], "managers", true ).should.be.true();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", "managers", true ).should.be.true();

		current.addRule( {
			selector: "feature.backup.remote.s3",
			user: "john.doe",
			accept: false,
		} );

		current.addRule( {
			selector: "feature.backup",
			role: "managers",
			accept: true,
		} );

		current.isAuthorized( "feature.backup.remote.s3", undefined, undefined ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", undefined ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", [] ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", undefined, "managers" ).should.be.true();
		current.isAuthorized( "feature.backup.remote.s3", [], "managers" ).should.be.true();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", "managers" ).should.be.false();

		current.isAuthorized( "feature.backup.remote.s3", undefined, undefined, true ).should.be.true();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", undefined, true ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", [], true ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", undefined, "managers", true ).should.be.true();
		current.isAuthorized( "feature.backup.remote.s3", [], "managers", true ).should.be.true();
		current.isAuthorized( "feature.backup.remote.s3", "john.doe", "managers", true ).should.be.false();
	} );

	it( "considers parent nodes on checking a user's authorization for accessing some specific resource", () => {
		const { current } = ctx.hitchy.api.service.AuthorizationTree;

		current.addRule( {
			selector: "feature",
			user: "john.doe",
			accept: true,
		} );

		current.addRule( {
			selector: "feature.backup",
			role: "manager",
			accept: true,
		} );

		current.addRule( {
			selector: "feature.backup.remote",
			user: "jane.doe",
			accept: false,
		} );

		current.addRule( {
			selector: "feature.backup.remote",
			role: ["supporters"],
			accept: true,
		} );

		current.isAuthorized( "feature.backup", "jane.doe", undefined, true ).should.be.true();
		current.isAuthorized( "feature.backup.remote", "jane.doe", undefined, true ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", "jane.doe", undefined, true ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", "jane.doe", ["supporters"], true ).should.be.false();
		current.isAuthorized( "feature.backup.remote.s3", "jim.doe", ["supporters"], true ).should.be.true();
	} );

	describe( "always rejects access on a particular resource when rules contradict each other on granting/revoking access to/from", () => {
		it( "user", () => {
			const { current } = ctx.hitchy.api.service.AuthorizationTree;

			current.isAuthorized( "feature.backup.remote", "jane.doe" ).should.be.false();
			current.isAuthorized( "feature.backup.remote", "jane.doe", undefined, true ).should.be.true();

			current.addRule( {
				selector: "feature.backup.remote",
				user: "jane.doe",
				accept: false,
			} );

			current.addRule( {
				selector: "feature.backup.remote",
				user: "jane.doe",
				accept: true,
			} );

			current.isAuthorized( "feature.backup.remote", "jane.doe" ).should.be.false();
			current.isAuthorized( "feature.backup.remote", "jane.doe", undefined, true ).should.be.false();
		} );

		it( "role", () => {
			const { current } = ctx.hitchy.api.service.AuthorizationTree;

			current.isAuthorized( "feature.backup.remote", undefined, "users" ).should.be.false();
			current.isAuthorized( "feature.backup.remote", undefined, "users", true ).should.be.true();

			current.addRule( {
				selector: "feature.backup.remote",
				role: "users",
				accept: false,
			} );

			current.addRule( {
				selector: "feature.backup.remote",
				role: "users",
				accept: true,
			} );

			current.isAuthorized( "feature.backup.remote", undefined, "users" ).should.be.false();
			current.isAuthorized( "feature.backup.remote", undefined, "users", true ).should.be.false();
		} );

		it( "user and/or role", () => {
			const { current } = ctx.hitchy.api.service.AuthorizationTree;

			current.isAuthorized( "feature.backup.remote", "jane.doe", "users" ).should.be.false();
			current.isAuthorized( "feature.backup.remote", "jane.doe", "users", true ).should.be.true();

			current.addRule( {
				selector: "feature.backup.remote",
				user: "jane.doe",
				accept: false,
			} );

			current.addRule( {
				selector: "feature.backup.remote",
				role: "users",
				accept: true,
			} );

			current.isAuthorized( "feature.backup.remote", "jane.doe", "users" ).should.be.false();
			current.isAuthorized( "feature.backup.remote", "jane.doe", "users", true ).should.be.false();
		} );
	} );

	describe( "can load rules from configuration", () => {
		describe( "mapping resource selectors into single user/role per rule which", () => {
			it( "is implicitly granting access on a resource to users", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "jane.doe",
						"feature.export.pdf": "         \n\r    john.doe\r"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: { "john.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is explicitly granting access on a resource to users", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "+jane.doe",
						"feature.export.pdf": "         \n\r  +  john.doe\r"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: { "john.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is revoking access on a resource from users", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "-jane.doe",
						"feature.export.pdf": "     \n -john.doe     \n\r "
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: { "jane.doe": 1 } } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: { "john.doe": 1 } } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is implicitly granting access on a resource to roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "@pro-users",
						"feature.export.pdf": "         \n\r    @all-users\r"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: { "pro-users": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "all-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is explicitly granting access on a resource to roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "+@pro-users",
						"feature.export.pdf": "         \n\r  +  @all-users \r"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: { "pro-users": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "all-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is revoking access on a resource from roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "-@pro-users",
						"feature.export.pdf": "     \n - @ all-users     \n\r "
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: { "pro-users": 1 } } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: { "all-users": 1 } } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is implicitly granting access on a resource to users/roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "jane.doe",
						"feature.export.pdf": "         \n\r    @all-users\r"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "all-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is explicitly granting access on a resource to users/roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "+jane.doe",
						"feature.export.pdf": "         \n\r  +  @all-users \r"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "all-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is revoking access on a resource from users/roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "-jane.doe",
						"feature.export.pdf": "     \n - @ all-users     \n\r "
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: { "jane.doe": 1 } } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: { "all-users": 1 } } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is revoking access on a resource from all users but those with a certain role", () => {
				const { AuthorizationTree: { current } } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						feature: "*",
						"feature.export": [ "-*", "@admin" ],
					}
				} );

				current.isAuthorized( "feature", undefined, undefined ).should.be.true();
				current.isAuthorized( "feature", { name: "jane.doe" }, undefined ).should.be.true();
				current.isAuthorized( "feature", { name: "jane.doe" }, ["user"] ).should.be.true();
				current.isAuthorized( "feature", undefined, ["admin"] ).should.be.true();
				current.isAuthorized( "feature", { name: "jane.doe" }, ["admin"] ).should.be.true();

				current.isAuthorized( "feature.import", undefined, undefined ).should.be.true();
				current.isAuthorized( "feature.import", { name: "jane.doe" }, undefined ).should.be.true();
				current.isAuthorized( "feature.import", { name: "jane.doe" }, ["user"] ).should.be.true();
				current.isAuthorized( "feature.import", undefined, ["admin"] ).should.be.true();
				current.isAuthorized( "feature.import", { name: "jane.doe" }, ["admin"] ).should.be.true();

				current.isAuthorized( "feature.export", undefined, undefined ).should.be.false();
				current.isAuthorized( "feature.export", { name: "jane.doe" }, undefined ).should.be.false();
				current.isAuthorized( "feature.export", { name: "jane.doe" }, ["user"] ).should.be.false();
				current.isAuthorized( "feature.export", undefined, ["admin"] ).should.be.true();
				current.isAuthorized( "feature.export", { name: "jane.doe" }, ["admin"] ).should.be.true();

				current.isAuthorized( "feature.export.pdf", undefined, undefined ).should.be.false();
				current.isAuthorized( "feature.export.pdf", { name: "jane.doe" }, undefined ).should.be.false();
				current.isAuthorized( "feature.export.pdf", { name: "jane.doe" }, ["user"] ).should.be.false();
				current.isAuthorized( "feature.export.pdf", undefined, ["admin"] ).should.be.true();
				current.isAuthorized( "feature.export.pdf", { name: "jane.doe" }, ["admin"] ).should.be.true();
			} );
		} );

		describe( "mapping resource selectors into string with comma-separated list of users/roles per rule which", () => {
			it( "is implicitly granting access on a resource to users", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "john.doe,jane-doe",
						"feature.export.pdf": "         \n\r    john.doe\r, jane.doe"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane-doe": 1, "john.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: { "jane.doe": 1, "john.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is explicitly granting access on a resource to users", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": " +john.doe,+ jane-doe",
						"feature.export.pdf": "         \n+\r    john.doe\r, +  jane.doe"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane-doe": 1, "john.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: { "jane.doe": 1, "john.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is revoking access on a resource from users", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": " -john.doe,- jane-doe",
						"feature.export.pdf": "         \n-\r    john.doe\r, -  jane.doe"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: { "john.doe": 1, "jane-doe": 1 } } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: { "john.doe": 1, "jane.doe": 1 } } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is implicitly granting access on a resource to roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "@pro-users,@b2b-users",
						"feature.export.pdf": "         \n\r    @all-users\r, @admin-users"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: { "pro-users": 1, "b2b-users": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "all-users": 1, "admin-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is explicitly granting access on a resource to roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "+@pro-users,+@b2b-users",
						"feature.export.pdf": "     +    \n\r    @all-users\r,+ @admin-users"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: { "pro-users": 1, "b2b-users": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "all-users": 1, "admin-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is revoking access on a resource from roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "-@pro-users,-@b2b-users",
						"feature.export.pdf": "     -    \n\r    @all-users\r,- @admin-users"
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: { "pro-users": 1, "b2b-users": 1 } } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: { "all-users": 1, "admin-users": 1 } } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is implicitly granting access on a resource to users/roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "jane.doe,@pro-users",
						"feature.export.pdf": "         \n\r    @pro-users\r,   jane.doe "
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: { "pro-users": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "pro-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is explicitly granting access on a resource to users/roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "+jane.doe,+@pro-users",
						"feature.export.pdf": "       +  \n\r    @pro-users\r,  + jane.doe "
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: { "pro-users": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "pro-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is revoking access on a resource from users/roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": "-jane.doe,-@pro-users",
						"feature.export.pdf": "       -  \n\r    @pro-users\r,  - jane.doe "
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: { "jane.doe": 1 } } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: { "pro-users": 1 } } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: { "jane.doe": 1 } } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: { "pro-users": 1 } } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );
		} );

		describe( "mapping resource selectors into array of users/roles per rule which", () => {
			it( "is implicitly granting access on a resource to users", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": [ "john.doe", "jane-doe" ],
						"feature.export.pdf": ["         \n\r    john.doe\r,   \rjane.doe "]
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane-doe": 1, "john.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: { "jane.doe": 1, "john.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is explicitly granting access on a resource to users", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": [ "+john.doe", "+jane-doe" ],
						"feature.export.pdf": ["         \n+\r    john.doe\r, +  \rjane.doe "]
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane-doe": 1, "john.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: { "jane.doe": 1, "john.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is revoking access on a resource from users", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": [ "-john.doe", "-jane-doe" ],
						"feature.export.pdf": ["      -   \n\r    john.doe\r,-   \rjane.doe "]
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: { "john.doe": 1, "jane-doe": 1 } } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: { "john.doe": 1, "jane.doe": 1 } } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is implicitly granting access on a resource to roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": [ "@pro-users", "@b2b-users" ],
						"feature.export.pdf": ["         \n\r    @all-users\r, @admin-users"]
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: { "pro-users": 1, "b2b-users": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "all-users": 1, "admin-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is explicitly granting access on a resource to roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": [ "+@pro-users", "+@b2b-users" ],
						"feature.export.pdf": ["        + \n\r    @all-users\r, +@admin-users"]
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: { "pro-users": 1, "b2b-users": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "all-users": 1, "admin-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is revoking access on a resource from roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": [ "-@pro-users", "-@b2b-users" ],
						"feature.export.pdf": ["         \n-\r    @all-users\r,- @admin-users"]
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: { "pro-users": 1, "b2b-users": 1 } } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: { "all-users": 1, "admin-users": 1 } } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is implicitly granting access on a resource to users/roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": [ "jane.doe", "@pro-users" ],
						"feature.export.pdf": ["         \n\r    @pro-users\r,   jane.doe "]
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: { "pro-users": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "pro-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is explicitly granting access on a resource to users/roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": [ "+jane.doe", "+@pro-users" ],
						"feature.export.pdf": ["         \n+\r    @pro-users\r, +  jane.doe "]
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: { "pro-users": 1 }, reject: {} } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: { "jane.doe": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: { "pro-users": 1 }, reject: {} } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );

			it( "is revoking access on a resource from users/roles", () => {
				const { AuthorizationTree: { current }, AuthorizationNode } = ctx.hitchy.api.service;

				current.loadFromConfiguration( {
					authorizations: {
						"feature.export": [ "-jane.doe", "-@pro-users" ],
						"feature.export.pdf": ["     -    \n\r    @pro-users\r,-   jane.doe "]
					}
				} );

				current.selectNode( "feature.export" ).users.should.be.deepEqual( { accept: {}, reject: { "jane.doe": 1 } } );
				current.selectNode( "feature.export" ).roles.should.be.deepEqual( { accept: {}, reject: { "pro-users": 1 } } );
				current.selectNode( "feature.export" ).children.should.have.size( 1 ).and.have.property( "pdf" ).which.is.an.instanceOf( AuthorizationNode );

				current.selectNode( "feature.export.pdf" ).users.should.be.deepEqual( { accept: {}, reject: { "jane.doe": 1 } } );
				current.selectNode( "feature.export.pdf" ).roles.should.be.deepEqual( { accept: {}, reject: { "pro-users": 1 } } );
				current.selectNode( "feature.export.pdf" ).children.should.be.empty();
			} );
		} );
	} );
} );
