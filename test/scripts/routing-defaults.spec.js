import { describe, it, before, after } from "mocha";
import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

import "should";
import "should-http";

import { getSID } from "../helper.js";

const Test = await SDT( Core );

describe( "The set of default routes in @hitchy/plugin-auth", () => {
	const ctx = {};
	let sid;

	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
		files: {
			"config/all.mjs": `
				export const policies = {
					before: {
						"/api/user": "authentication.mustBeAuthenticated",
					},
				};

				export const routes = {
					"/api/user": ( _, res ) => res.json( { success: true } ),
				};
			`,
		},
	} ) );

	after( Test.after( ctx ) );

	it( "is running", async() => {
		const res = await ctx.get( "/api/user" );

		res.should.have.status( 403 );
		res.headers.should.have.property( "set-cookie" ).which.is.an.Array().which.is.not.empty();

		sid = getSID( res.headers );
		sid.should.be.ok();
	} );

	it( "provides information on missing user authentication", async() => {
		const res = await ctx.get( "/api/auth/current", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.headers.should.not.have.property( "set-cookie" );
		res.data.should.be.Object().which.has.properties( "success", "authenticated" );
		res.data.success.should.be.true();
		res.data.authenticated.should.be.false();
	} );

	it( "supports authentication of default user using POSTed form data", async() => {
		const res = await ctx.post( "/api/auth/login", "username=admin&password=nimda", {
			cookie: `sessionId=${sid}`,
			"Content-Type": "application/x-www-form-urlencoded",
		} );

		res.should.have.status( 200 );
		res.headers.should.have.property( "set-cookie" );
		res.data.success.should.be.true();

		sid = getSID( res.headers );
		sid.should.be.ok();
	} );

	it( "provides information on previously authenticated user due to tracking user in server-side session", async() => {
		const res = await ctx.get( "/api/auth/current", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.headers.should.not.have.property( "set-cookie" );
		res.data.should.be.Object().which.has.properties( "success", "authenticated" );
		res.data.success.should.be.true();
		res.data.authenticated.should.not.be.false();
	} );

	it( "does not provide information on previously authenticated user when requesting w/o selection of previously provided session ID", async() => {
		const res = await ctx.get( "/api/auth/current" );

		res.should.have.status( 200 );
		res.headers.should.have.property( "set-cookie" ).which.is.an.Array().which.is.not.empty();
		res.data.should.be.Object().which.has.properties( "success", "authenticated" );
		res.data.success.should.be.true();
		res.data.authenticated.should.be.false();

		getSID( res.headers ).should.be.ok().and.not.equal( sid );
	} );

	it( "does not drop information on previously authenticated user when requesting in context of proper session again", async() => {
		const res = await ctx.get( "/api/auth/current", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.headers.should.not.have.property( "set-cookie" );
		res.data.should.be.Object().which.has.properties( "success", "authenticated" );
		res.data.success.should.be.true();
		res.data.authenticated.should.not.be.false();
	} );

	it( "handles request for dropping authenticated user", async() => {
		const res = await ctx.get( "/api/auth/logout", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.headers.should.have.property( "set-cookie" );

		const newSID = getSID( res.headers );
		newSID.should.be.ok().and.not.equal( sid );
		sid = newSID;
	} );

	it( "rejects access on REST API endpoint /api/user due to lack of authentication", async() => {
		const res = await ctx.get( "/api/user", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 403 );
	} );

	it( "supports authentication of default user using POSTed JSON data", async() => {
		const res = await ctx.post( "/api/auth/login", { username: "admin", password: "nimda" }, {
			cookie: `sessionId=${sid}`,
			"Content-Type": "application/json",
		} );

		res.should.have.status( 200 );
		res.headers.should.have.property( "set-cookie" );
		res.data.success.should.be.true();

		sid = getSID( res.headers );
		sid.should.be.ok();
	} );

	it( "provides information on previously re-authenticated user due to tracking user in server-side session", async() => {
		const res = await ctx.get( "/api/auth/current", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.headers.should.not.have.property( "set-cookie" );
		res.data.should.be.Object().which.has.properties( "success", "authenticated" );
		res.data.success.should.be.true();
		res.data.authenticated.should.not.be.false();
	} );

	it( "grants access on REST API endpoint /api/user due to previous authentication as administrative user", async() => {
		const res = await ctx.get( "/api/user", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.data.should.be.Object().which.has.property( "success" ).which.is.true();
	} );

	it( "supports authentication of default user dropped using GET request", async() => {
		const res = await ctx.get( "/api/auth/logout", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.headers.should.have.property( "set-cookie" );
		res.data.success.should.be.true();

		const newSID = getSID( res.headers );
		newSID.should.be.ok().and.not.equal( sid );
		sid = newSID;
	} );

	it( "revokes access on REST API endpoint /api/user due to previous session and its authentication being dropped", async() => {
		const res = await ctx.get( "/api/user", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 403 );
		res.headers.should.not.have.property( "set-cookie" );
	} );
} );

