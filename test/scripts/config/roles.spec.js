import { describe, it, before, after } from "mocha";
import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

import "should";

const Test = await SDT( Core );

describe( "config.auth.roles", () => {
	const ctx = {};

	describe( "can be omitted and thus @hitchy/plugin-auth", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"config/all.cjs": `
					exports.auth = {};
				`,
			},
		} ) );

		it( "is declaring role `admin` by default, only", async() => {
			const { Role } = ctx.hitchy.api.models;

			const roles = await Role.list();

			roles.should.be.an.Array().which.has.length( 1 );
			roles[0].name.should.equal( "admin" );
		} );
	} );

	describe( "can be omitted even though custom admin role is configured and thus @hitchy/plugin-auth", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"config/all.cjs": `
					exports.auth = {
						admin: {
							role: "hero"
						},
					};
				`,
			},
		} ) );

		it( "is declaring that custom admin role by default, only", async() => {
			const { Role } = ctx.hitchy.api.models;

			const roles = await Role.list();

			roles.should.be.an.Array().which.has.length( 1 );
			roles[0].name.should.equal( "hero" );
		} );
	} );

	describe( "may be list of role names and thus @hitchy/plugin-auth", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"config/all.cjs": `
					exports.auth = {
						roles: [ "guest", "customer", "manager" ],
					};
				`,
			},
		} ) );

		it( "is declaring those roles in addition to the default admin role", async() => {
			const { Role } = ctx.hitchy.api.models;

			const roles = await Role.list();

			roles.should.be.an.Array().which.has.length( 4 );
			roles.map( role => role.name ).should.containEql( "admin" );
			roles.map( role => role.name ).should.containEql( "guest" );
			roles.map( role => role.name ).should.containEql( "customer" );
			roles.map( role => role.name ).should.containEql( "manager" );
		} );
	} );

	describe( "may include name of admin role and thus @hitchy/plugin-auth", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"config/all.cjs": `
					exports.auth = {
						roles: [ "guest", "admin", "customer", "manager" ],
					};
				`,
			},
		} ) );

		it( "is declaring all listed roles ignoring additionally listed admin role", async() => {
			const { Role } = ctx.hitchy.api.models;

			const roles = await Role.list();

			roles.should.be.an.Array().which.has.length( 4 );
			roles.map( role => role.name ).should.containEql( "admin" );
			roles.map( role => role.name ).should.containEql( "guest" );
			roles.map( role => role.name ).should.containEql( "customer" );
			roles.map( role => role.name ).should.containEql( "manager" );
		} );
	} );

	describe( "may be list of role names and thus @hitchy/plugin-auth", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"config/all.cjs": `
					exports.auth = {
						admin: { role: "hero" },
						roles: [ "guest", "customer", "manager" ],
					};
				`,
			},
		} ) );

		it( "is declaring those roles in addition to some custom admin role", async() => {
			const { Role } = ctx.hitchy.api.models;

			const roles = await Role.list();

			roles.should.be.an.Array().which.has.length( 4 );
			roles.map( role => role.name ).should.containEql( "hero" );
			roles.map( role => role.name ).should.containEql( "guest" );
			roles.map( role => role.name ).should.containEql( "customer" );
			roles.map( role => role.name ).should.containEql( "manager" );
		} );
	} );

	describe( "may include name of custom admin role and thus @hitchy/plugin-auth", () => {
		after( Test.after( ctx ) );
		before( Test.before( ctx, {
			plugin: true,
			projectFolder: false,
			files: {
				"config/all.cjs": `
					exports.auth = {
						admin: { role: "hero" },
						roles: [ "guest", "hero", "customer", "manager" ],
					};
				`,
			},
		} ) );

		it( "is declaring all listed roles ignoring additionally listed custom admin role", async() => {
			const { Role } = ctx.hitchy.api.models;

			const roles = await Role.list();

			roles.should.be.an.Array().which.has.length( 4 );
			roles.map( role => role.name ).should.containEql( "hero" );
			roles.map( role => role.name ).should.containEql( "guest" );
			roles.map( role => role.name ).should.containEql( "customer" );
			roles.map( role => role.name ).should.containEql( "manager" );
		} );
	} );

	describe( "must not contain names that do not comply with naming rules per role and thus @hitchy/plugin-auth", () => {
		after( Test.after( ctx ) );

		it( "is crashing the application on start", async() => {
			await Test.before( ctx, {
				plugin: true,
				projectFolder: false,
				files: {
					"config/all.mjs": `
						export const auth = {
							roles: [ ".guest", "cus tomer", "1manager" ],
						};
					`,
				},
			} )().should.be.rejected();
		} );
	} );
} );
