import { describe, it, before, after } from "mocha";
import "should";
import "should-http";

import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

const Test = await SDT( Core );

describe( "AuthenticationPolicy", () => {
	const ctx = {};

	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
		files: {
			"config/all.cjs": `
				"use strict";
				
				exports.policies = {
					before: {
						"/api/user": "authorization.mustBeAdmin",
					},
				};
				
				exports.routes = {
					"/api/user{/:action}{/:uuid}": ( _, res ) => res.json( { success: true } ),
				};
			`,
		},
	} ) );
	after( Test.after( ctx ) );

	describe( "supports HTTP Basic authentication which", () => {
		it( "does not authenticate any user by default", async() => {
			const res = await ctx.get( "/api/user" );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "x-authenticated-as" );
			res.headers.should.not.have.property( "x-authorized-as" );
		} );

		it( "is ignoring a completely malformed authorization header", async() => {
			const res = await ctx.get( "/api/user", {
				authorization: "foo",
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "x-authenticated-as" );
			res.headers.should.not.have.property( "x-authorized-as" );
		} );

		it( "is ignoring a more subtly malformed Basic authorization header", async() => {
			const res = await ctx.get( "/api/user", {
				authorization: "basic aaaa",
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "x-authenticated-as" );
			res.headers.should.not.have.property( "x-authorized-as" );
		} );

		it( "is failing authentication on providing well formed Basic authorization header with unknown username", async() => {
			const res = await ctx.get( "/api/user", {
				authorization: "basic " + Buffer.from( "admin2:nimda" ).toString( "base64" ),
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "x-authenticated-as" );
			res.headers.should.not.have.property( "x-authorized-as" );
		} );

		it( "is failing on providing well formed Basic authorization header with proper username but wrong password", async() => {
			const res = await ctx.get( "/api/user", {
				authorization: "basic " + Buffer.from( "admin:admin" ).toString( "base64" ),
			} );

			res.should.have.status( 403 );
			res.headers.should.not.have.property( "x-authenticated-as" );
			res.headers.should.not.have.property( "x-authorized-as" );
		} );

		it( "is succeeding on providing well formed Basic authorization header with proper username and password", async() => {
			const res = await ctx.get( "/api/user", {
				authorization: "basic " + Buffer.from( "admin:nimda" ).toString( "base64" ),
			} );

			res.should.have.status( 200 );
			res.headers.should.have.property( "x-authenticated-as" ).which.is.equal( "admin" );
			res.headers.should.have.property( "x-authorized-as" ).which.is.equal( "admin" );
		} );
	} );
} );
