import { describe, it, before, after } from "mocha";
import "should";
import "should-http";

import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

import { getSID } from "../../helper.js";

const Test = await SDT( Core );

describe( "AuthorizationPolicy", () => {
	const ctx = {};
	let sid;

	before( Test.before( ctx, {
		plugin: true,
		projectFolder: false,
		files: {
			"config/all.cjs": `
				"use strict";
				
				exports.policies = {
					before: {
						"/api/user": "authorization.mustBeAdmin",
					},
				};
				
				exports.routes = {
					"/api/user{/:action}{/:uuid}": ( _, res ) => res.json( { success: true } ),
				};
			`,
		},
	} ) );

	after( Test.after( ctx ) );

	it( "rejects unauthenticated request on route protected with mustBeAdmin()", async() => {
		const res = await ctx.get( "/api/user" );

		res.should.have.status( 403 );
		res.headers.should.have.property( "set-cookie" ).which.is.an.Array().which.is.not.empty();

		sid = getSID( res.headers );
		sid.should.be.ok();
	} );

	it( "provides information on missing user authentication", async() => {
		const res = await ctx.get( "/api/auth/current", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.headers.should.not.have.property( "set-cookie" );
		res.data.should.be.Object().which.has.properties( "success", "authenticated" );
		res.data.success.should.be.true();
		res.data.authenticated.should.be.false();
	} );

	it( "denies unauthenticated access on routes protected with mustBeAdmin() policy", async() => {
		const { User } = ctx.hitchy.api.models;

		const list = await User.list();
		const uuid = list[0].uuid;

		let res = await ctx.get( "/api/user", { cookie: `sessionId=${sid}` } );
		res.should.have.status( 403 );
		res.data.error.should.be.startWith( "access forbidden" );

		res = await ctx.get( "/api/user/" + uuid, { cookie: `sessionId=${sid}` } );
		res.should.have.status( 403 );
		res.data.error.should.be.startWith( "access forbidden" );

		res = await ctx.get( "/api/user/write/" + uuid, { cookie: `sessionId=${sid}` } );
		res.should.have.status( 403 );
		res.data.error.should.be.startWith( "access forbidden" );

		res = await ctx.get( "/api/user/replace/" + uuid, { cookie: `sessionId=${sid}` } );
		res.should.have.status( 403 );
		res.data.error.should.be.startWith( "access forbidden" );

		res = await ctx.put( "/api/user/" + uuid, null, { cookie: `sessionId=${sid}` } );
		res.should.have.status( 403 );
		res.data.error.should.be.startWith( "access forbidden" );

		res = await ctx.patch( "/api/user/" + uuid, null, { cookie: `sessionId=${sid}` } );
		res.should.have.status( 403 );
		res.data.error.should.be.startWith( "access forbidden" );
	} );

	it( "allows access on routes protected with mustBeAdmin() policy when client is authenticated as admin", async() => {
		const { User } = ctx.hitchy.api.models;

		let res = await ctx.post( "/api/auth/login", "username=admin&password=nimda", {
			cookie: `sessionId=${sid}`,
			"Content-Type": "application/x-www-form-urlencoded",
		} );

		res.should.have.status( 200 );
		res.headers.should.have.property( "set-cookie" );
		res.data.success.should.be.true();

		sid = getSID( res.headers );
		sid.should.be.ok();

		const list = await User.list();
		const uuid = list[0].uuid;

		res = await ctx.get( "/api/user", { cookie: `sessionId=${sid}` } );
		res.should.have.status( 200 );

		res = await ctx.get( "/api/user/" + uuid, { cookie: `sessionId=${sid}` } );
		res.should.have.status( 200 );

		res = await ctx.get( "/api/user/write/" + uuid, { cookie: `sessionId=${sid}` } );
		res.should.have.status( 200 );

		res = await ctx.get( "/api/user/replace/" + uuid, { cookie: `sessionId=${sid}` } );
		res.should.have.status( 200 );

		res = await ctx.put( "/api/user/" + uuid, null, { cookie: `sessionId=${sid}` } );
		res.should.have.status( 200 );

		res = await ctx.patch( "/api/user/" + uuid, null, { cookie: `sessionId=${sid}` } );
		res.should.have.status( 200 );
	} );
} );
