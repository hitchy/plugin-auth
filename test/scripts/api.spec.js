import { describe, it, before, after } from "mocha";
import "should";

import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

const Test = await SDT( Core );

describe( "authentication plugin", () => {
	const ctx = {};

	before( Test.before( ctx, { plugin: true, projectFolder: false } ) );
	after( Test.after( ctx ) );

	it( "introduces some custom controllers", () => {
		const { controllers } = ctx.hitchy.api;

		controllers.User.should.be.ok();
	} );

	it( "introduces some custom policies", () => {
		const { policies } = ctx.hitchy.api;

		policies.Authentication.should.be.ok();
		policies.Authorization.should.be.ok();
		policies.User.should.be.ok();
	} );

	it( "introduces some custom models", () => {
		const { models } = ctx.hitchy.api;

		models.AuthorizationRule.should.be.ok();
		models.User.should.be.ok();
		models.Role.should.be.ok();
	} );

	it( "introduces some custom services", () => {
		const { services } = ctx.hitchy.api;

		services.AuthenticationPassport.should.be.ok();
		services.AuthenticationStrategies.should.be.ok();

		services.Authorization.should.be.ok();
		services.AuthorizationNode.should.be.ok();
		services.AuthorizationTree.should.be.ok();
		services.AuthorizationPolicyGenerator.should.be.ok();
	} );
} );
