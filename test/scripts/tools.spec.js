import Path from "node:path";
import { spawn } from "node:child_process";
import { fileURLToPath } from "node:url";

import { describe, it } from "mocha";
import "should";

describe( "plugin-auth has a tool hash-password which", () => {
	const runHashPassword = input => new Promise( ( resolve, reject ) => {
		const child = spawn( "node hash-password.js", {
			shell: true,
			cwd: Path.resolve( fileURLToPath( import.meta.url ), "../../.." ),
		} );

		const stdout = [];
		const stderr = [];

		child.once( "error", reject );

		child.stdout.on( "data", chunk => stdout.push( chunk ) );
		child.stderr.on( "data", chunk => stderr.push( chunk ) );
		child.once( "close", exitCode => resolve( {
			stdout: Buffer.concat( stdout ).toString( "utf-8" ),
			stderr: Buffer.concat( stderr ).toString( "utf-8" ),
			exitCode
		} ) );

		child.stdin.end( input );
	} );

	it( "can be invoked without input", async() => {
		const { exitCode } = await runHashPassword( "" );

		exitCode.should.equal( 0 );
	} );

	it( "does not generate any output by default", async() => {
		const { stdout } = await runHashPassword( "" );

		stdout.should.equal( "" );
	} );

	it( "generates error when invoking without any input", async() => {
		const { stderr } = await runHashPassword( "" );

		stderr.should.match( /missing\b.+\bpassword/ );
	} );

	it( "can be invoked with input", async() => {
		const { exitCode } = await runHashPassword( "f" );

		exitCode.should.equal( 0 );
	} );

	it( "generates a hash for any provided input string", async() => {
		const { stdout } = await runHashPassword( "f" );

		stdout.should.not.be.empty();
		stdout.should.not.equal( "f" );
		stdout.trim().length.should.be.greaterThan( 32 );
		stdout.trim().should.not.match( /\s/ );
	} );

	it( "does not generate any error when invoking with some input", async() => {
		const { stderr } = await runHashPassword( "f" );

		stderr.should.not.match( /missing\b.+\bpassword/ );
	} );
} );
