import { describe, before, after, it } from "mocha";
import Core from "@hitchy/core/sdt.js";
import SDT from "@hitchy/server-dev-tools";

import "should";
import "should-http";

import { getSID } from "../helper.js";

const Test = await SDT( Core );

describe( "Integration of passport's support for OpenID Connect with Authorization Code Flow", function() {
	this.timeout( 5000 );

	const ctx = {};
	let sid = null;

	before( function() {
		this.timeout( 5000 );

		return Test.before( ctx, {
			plugin: true,
			projectFolder: "../project/testbed",
		} )();
	} );

	after( Test.after( ctx ) );

	it( "has strategy in runtime configuration", () => {
		ctx.hitchy.api.config.auth.strategies.should.have.property( "oidc" ).which.is.an.Object();
	} );

	it( "is running", async() => {
		const res = await ctx.get( "/api/auth/current" );

		res.should.have.status( 200 );
		res.headers.should.have.property( "set-cookie" ).which.is.an.Array().which.is.not.empty();
	} );

	it( "starts session on instant request for logging in", async() => {
		const res = await ctx.post( "/api/auth/login/oidc" );

		res.headers.should.have.property( "set-cookie" ).which.is.an.Array().which.is.not.empty();

		sid = getSID( res.headers );
		sid.should.be.ok();
	} );

	it( "redirects to IdP on a instant request for logging in", async() => {
		const res = await ctx.post( "/api/auth/login/oidc" );

		res.should.have.status( 302 );
		res.headers.location.should.startWith( "https://idp.cepharum.de/auth/realms/hitchy-plugin-auth-ci-test/protocol/openid-connect/auth?" );
		res.headers.should.have.property( "set-cookie" ).which.is.an.Array().which.is.not.empty();

		sid = getSID( res.headers );
		sid.should.be.ok();
	} );

	it( "redirects to IdP on a follow-up request for logging in within same session", async() => {
		const res = await ctx.post( "/api/auth/login/oidc", "", {
			cookie: `sessionId=${sid}`
		} );

		res.should.have.status( 302 );
		res.headers.location.should.startWith( "https://idp.cepharum.de/auth/realms/hitchy-plugin-auth-ci-test/protocol/openid-connect/auth?" );
		res.headers.should.not.have.property( "set-cookie" );
	} );

	it( "returns after logging in at IdP", async() => {
		const local = await ctx.post( "/api/auth/login/oidc", "", {
			cookie: `sessionId=${sid}`
		} );

		local.should.have.status( 302 );
		local.headers.should.not.have.property( "set-cookie" );

		const client = ctx.hitchy.api.services.HttpClient;
		const firstRemote = await client.fetch( "GET", local.headers.location );

		firstRemote.should.have.status( 200 );

		const html = ( await firstRemote.body() ).toString();
		const [ , formUrl ] = html.match( /<form[^>]+?\baction="([^"]+)"/ );
		const cookies = firstRemote.headers["set-cookie"].map( rule => rule.replace( /;[\s\S]*$/, "" ) );

		const secondRemote = await client.fetch( "POST", formUrl.replace( /&amp;/gi, "&" ),
			"username=hitchy-plugin-auth-test-user&password=hitchy-plugin-auth-test-password&credentialId=", {
				"Content-Type": "application/x-www-form-urlencoded",
				Cookie: cookies.join( "; " ),
			} );

		secondRemote.should.have.status( 302 );
		secondRemote.headers.location.should.startWith( "http://localhost:3000/api/auth/login/oidc" );

		const second = await ctx.get( secondRemote.headers.location.substring( "http://localhost:3000".length ), {
			cookie: `sessionId=${sid}`
		} );

		second.should.have.status( 200 );
		second.headers.should.have.property( "set-cookie" ).which.is.an.Array().which.is.not.empty();

		sid = getSID( second.headers );
		sid.should.be.ok();
	} );

	it( "provides information on previously re-authenticated user due to tracking user in server-side session", async() => {
		const res = await ctx.get( "/api/auth/current", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.headers.should.not.have.property( "set-cookie" );
		res.data.should.be.Object().which.has.properties( "success", "authenticated" );
		res.data.success.should.be.true();
		res.data.authenticated.should.not.be.false();
	} );

	it( "grants access on REST API endpoint /api/user due to previous authentication as administrative user", async() => {
		const res = await ctx.get( "/api/user", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.data.should.be.Object().which.has.property( "success" ).which.is.true();
	} );

	it( "supports authenticated user being dropped using GET request", async function() {
		this.timeout( 20000 );

		const { HttpClient } = ctx.hitchy.api.services;

		const logoutRes = await ctx.get( "/api/auth/logout", {
			cookie: `sessionId=${sid}`,
		} );

		logoutRes.should.have.status( 302 );
		logoutRes.headers.should.not.have.property( "set-cookie" );
		logoutRes.headers.should.have.property( "location" );

		const idpRes = await HttpClient.fetch( "GET", logoutRes.headers.location );

		idpRes.should.have.status( 302 );
		idpRes.headers.should.have.property( "location" );

		const finalRes = await ctx.get( idpRes.headers.location.replace( /^.*?\/\/[^/]*/, "" ), {
			cookie: `sessionId=${sid}`,
		} );

		finalRes.should.have.status( 200 );
	} );

	it( "rejects to know server-side session used for authentication previously after logging out", async() => {
		const res = await ctx.get( "/api/auth/current", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 200 );
		res.headers.should.have.property( "set-cookie" );
		res.data.should.be.Object().which.has.properties( "success", "authenticated" );
		res.data.success.should.be.true();
		res.data.authenticated.should.be.false();
	} );

	it( "revokes access on REST API endpoint /api/user due to previous session and its authentication being dropped", async() => {
		const res = await ctx.get( "/api/user", {
			cookie: `sessionId=${sid}`,
		} );

		res.should.have.status( 403 );
		res.headers.should.have.property( "set-cookie" );
	} );
} );
