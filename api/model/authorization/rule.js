export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { services } = api;

	/**
	 * Implements properties and behavior of a single authorization rule
	 * describing whether some user and/or role is authorized to access some
	 * selected resource.
	 *
	 * @property {string} selector full-stop-separated path of a resource in a hierarchy of resources
	 * @property {Buffer} user UUID of user this rule is affecting
	 * @property {Buffer} role UUID of role this rule is affecting
	 * @property {boolean} accept true if user/role is granted access on selected resource, false otherwise
	 *
	 * @name Hitchy.Plugin.Auth.AuthorizationRule
	 */
	return {
		props: {
			selector: { type: "string", required: true },
			user: { type: "uuid" },
			role: { type: "uuid" },
			accept: { type: "boolean", default: true },
		},
		hooks: {
			afterSave( existsAlready ) {
				const cache = services.AuthorizationTree.current;

				if ( existsAlready ) {
					const { changed: previous } = this.$properties.$context;

					cache.removeRule( {
						selector: previous.has( "selector" ) ? previous.get( "selector" ) : this.selector,
						role: previous.has( "role" ) ? previous.get( "role" ) : this.role,
						user: previous.has( "user" ) ? previous.get( "user" ) : this.user,
						accept: previous.has( "accept" ) ? previous.get( "accept" ) : this.accept,
					} );
				}

				return cache.addRule( this );
			},

			afterRemove() {
				return services.AuthorizationTree.current.removeRule( this );
			},
		},
	};
}
