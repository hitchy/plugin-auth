import Crypto from "node:crypto";

export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { services, models } = api;

	/**
	 * Implements model of a user suitable for authenticating as.
	 *
	 * @property {string} name unique name of user
	 * @property {string} role name of user's role (user-role relationship is 1:n)
	 * @property {string} password hash of user's password required for authenticating as
	 * @property {string} strategy name of passport strategy used for authentication
	 * @property {string} strategyData additional information specific to strategy used
	 *
	 * @name Hitchy.Plugin.Auth.User
	 */
	return {
		props: {
			name: {
				required: true,
				unique: true,
			},
			password: {
				private: true,
			},
			strategy: {
				private: true,
			},
			strategyData: {
				private: true,
			},
		},
		hooks: {
			afterValidate( errors ) {
				if ( !errors.length ) {
					const strategy = this.strategy || services.AuthenticationStrategies.defaultStrategy();

					switch ( strategy ) {
						case "local" :
							if ( this.password ) break;

						// falls through
						default :
							if ( !api.config.auth.strategies[strategy].passwordRequired || this.password ) {
								break;
							}

							errors.push( new TypeError( "password required" ) );
					}
				}

				return errors;
			},

			async beforeSave( existsAlready, record ) {
				if ( record.password ) {
					record.password = await this.hashPassword( record.password );
				}

				return record;
			},

			async afterRemove() {
				const associations = await models.UserToRole.find( { eq: { name: "user", value: this.$uuid } } );

				await Promise.all( associations.map( association => association.remove() ) );
			},
		},
		methods: {
			/**
			 * Derives salted hash from provided password.
			 *
			 * @param {Buffer|string} cleartext cleartext password to be hashed
			 * @param {Buffer|string} previous previously derived hash containing salt to use again for comparing results
			 * @returns {Promise<string>} promise for salted hash of provided cleartext password
			 */
			async hashPassword( cleartext, previous = null ) {
				const normalized = Buffer.isBuffer( cleartext ) ? cleartext.toString( "utf8" ) : typeof cleartext === "string" ? cleartext : null;
				if ( !normalized ) {
					throw new TypeError( "missing or invalid cleartext password to be hashed" );
				}

				if ( parseHashed( cleartext ) ) {
					// `cleartext` is actually some previously derived hash already -> pass
					return cleartext;
				}

				const parsedPrevious = parseHashed( previous );
				const algorithmName = parsedPrevious && parsedPrevious.algorithm;
				let salt;

				if ( parsedPrevious && parsedPrevious.salt ) {
					salt = parsedPrevious.salt;
				} else {
					salt = await randomOctets( await randomNumber( 16, 32 ) );
				}

				switch ( algorithmName || "SCRYPT" ) {
					case "SSHA512" : {
						const hash = Crypto.createHash( "SHA512" );
						hash.update( normalized );
						hash.update( salt );

						return "{SSHA512}" + Buffer.concat( [ hash.digest(), salt ] ).toString( "base64" );
					}

					case "SCRYPT" :
						return await new Promise( ( resolve, reject ) => Crypto.scrypt( normalized, salt, 64, {
							cost: 16384,
							blockSize: 8,
							parallelization: 1,
						}, ( error, hash ) => {
							if ( error ) {
								reject( error );
							} else {
								resolve( "{SCRYPT}" + Buffer.concat( [ hash, salt ] ).toString( "base64" ) );
							}
						} ) );

					default :
						throw new TypeError( `unsupported keyword hashing algorithm: ${algorithmName}` );
				}

				/**
				 * Extracts elements of provided hash.
				 *
				 * @param {string|Buffer} input some hash to be parsed
				 * @returns {object} extracted elements of provided hash, nullish if extraction failed
				 */
				function parseHashed( input ) {
					const _input = Buffer.isBuffer( input ) ? input.toString( "utf8" ) : typeof input === "string" ? input : null;

					if ( _input ) {
						const match = /^{([a-z0-9]+)}/i.exec( _input );

						if ( match ) {
							const algorithm = match[1].toUpperCase();
							const raw = Buffer.from( _input.slice( algorithm.length + 2 ), "base64" );
							let hash, salt; // eslint-disable-line no-shadow

							switch ( algorithm ) {
								case "SSHA512" :
								case "SCRYPT" :
									hash = raw.slice( 0, 64 );
									salt = raw.slice( 64 );
									break;

								default :
									hash = raw;
									salt = null;
							}

							return { algorithm, raw, hash, salt, compiled: `{${algorithm}` + raw.toString( "base64" ) };
						}
					}

					return null;
				}

				/**
				 * Promises random integer in selected range.
				 *
				 * @param {number} min minimum value (inclusive)
				 * @param {number} max maximum value (exclusive)
				 * @returns {Promise<number>} promise for random integer in selected range
				 */
				function randomNumber( min, max ) {
					return new Promise( ( resolve, reject ) => Crypto.randomInt( min, max, ( error, number ) => {
						if ( error ) {
							reject( error );
						} else {
							resolve( number );
						}
					} ) );
				}

				/**
				 * Promises selected number of octets with random values.
				 *
				 * @param {number} size number of octets to generate
				 * @returns {Promise<Buffer>} promise for selected number of random octets
				 */
				function randomOctets( size ) {
					return new Promise( ( resolve, reject ) => Crypto.randomBytes( size, ( error, octets ) => {
						if ( error ) {
							reject( error );
						} else {
							resolve( octets );
						}
					} ) );
				}
			},

			/**
			 * Derives and saves salted hash from provided password.
			 *
			 * @param {string} password cleartext password to be hashed and saved
			 * @return {Promise<string>} promises derived hash set as password
			 */
			async setPassword( password ) {
				const hashed = await this.hashPassword( password );

				this.password = hashed;

				return hashed;
			},

			/**
			 * Derives and compares salted hash from provided password with
			 * saved password.
			 *
			 * @param {string} password cleartext password to be hashed and verified with saved one
			 * @return {Promise<boolean>} promises true if password hashes match
			 */
			async verifyPassword( password ) {
				await this.load();

				return this.password === await this.hashPassword( password, this.password );
			},
		},
	};
}
