export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { models } = api;

	/**
	 * Implements model of a role users can have for common authorization.
	 *
	 * @property {string} name unique name of user
	 *
	 * @name Hitchy.Plugin.Auth.Role
	 * @type {Hitchy.Plugin.Odem.Model}
	 */
	return {
		props: {
			name: {
				required: true,
				unique: true,
			},
		},
		hooks: {
			async afterRemove() {
				const associations = await models.UserToRole.find( { eq: { name: "role", value: this.$uuid } } );

				await Promise.all( associations.map( association => association.remove() ) );
			},
		},
		methods: {
			toString() {
				return this.name;
			},
		},
	};
}
