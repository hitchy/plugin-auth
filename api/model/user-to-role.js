/**
	 * Implements model of a user suitable for authenticating as.
	 *
	 * @property {Buffer} role UUID of associated role
	 * @property {Buffer} user UUID of associated user
	 *
	 * @name Hitchy.Plugin.Auth.UserToRole
	 */
export default {
	props: {
		user: {
			required: true,
			type: "uuid",
		},
		role: {
			required: true,
			type: "uuid",
		},
	},
};
