/**
 * Implements request handlers for common requests related to authenticating as
 * a user.
 */
export default class UserController {
	/**
	 * Responds on request for changing authenticated user's password.
	 *
	 * This handler assumes to follow one or more policy handler(s) which
	 * actually manage the process of changing the password.
	 *
	 * @param {Hitchy.Core.IncomingMessage} req request descriptor
	 * @param {Hitchy.Core.ServerResponse} res response manager
	 * @returns {void}
	 */
	static changePassword( req, res ) {
		res.format( {
			default() {
				res.json( {
					success: true
				} );
			},
		} );
	}

	/**
	 * Responds on success after authenticating as a user.
	 *
	 * This request handler assumes to follow one or more policy handler(s)
	 * which actually manage authentication.
	 *
	 * @param {Hitchy.Core.IncomingMessage} req request descriptor
	 * @param {Hitchy.Core.ServerResponse} res response manager
	 * @returns {void}
	 */
	static authenticate( req, res ) {
		res.format( {
			default() {
				res.json( {
					success: true,
					authenticated: Boolean( req.user ),
				} );
			},
		} );
	}

	/**
	 * Provides status of current request's user being authenticated or not.
	 *
	 * @param {Hitchy.Core.IncomingMessage} req request descriptor
	 * @param {Hitchy.Core.ServerResponse} res response manager
	 * @returns {void}
	 */
	static getCurrent( req, res ) {
		res.format( {
			default() {
				res.json( {
					success: true,
					authenticated: req.user ? {
						uuid: req.user.uuid,
						name: req.user.name,
						strategy: req.user.strategy || "local",
						roles: req.user.roles.map( role => role.name ),
					} : false,
				} );
			},
		} );
	}

	/**
	 * Drops authentication of current request's user. This request handler
	 * is assuming to be a follow-up handler to some policy actually dropping
	 * the user's authentication.
	 *
	 * @param {Hitchy.Core.IncomingMessage} req request descriptor
	 * @param {Hitchy.Core.ServerResponse} res response manager
	 * @returns {void}
	 */
	static unauthenticate( req, res ) {
		res.format( {
			default() {
				res.json( {
					success: true,
				} );
			},
		} );
	}

	static useCMP = false;
}
