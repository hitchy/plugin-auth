export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { models, service } = api;

	const logAlert = api.log( "hitchy:auth:alert" );
	const logDebug = api.log( "hitchy:auth:debug" );

	/**
	 * Implements policy handlers transparently managing authentication process
	 * of requesting user.
	 */
	class AuthenticationPolicy {
		/**
		 * Integrates passport and server-side session with provided request.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next invoke to continue request handling
		 * @returns {void}
		 */
		static injectPassport( req, res, next ) {
			if ( this.local.passportInjected ) {
				next();
				return;
			}

			this.local.passportInjected = true;

			const { AuthenticationPassport } = service;

			AuthenticationPassport.initialize()( req, res, initializeError => {
				if ( initializeError ) {
					next( initializeError );
				} else {
					AuthenticationPassport.session()( req, res, sessionError => {
						if ( req.user ) {
							const { name, roles } = req.user;

							res.set( "X-Authenticated-As", name );
							res.set( "X-Authorized-As", roles.join( "," ) );
						}

						this.updateSessionId( req, res, error => next( sessionError || error ) );
					} );
				}
			} );
		}

		/**
		 * Discovers HTTP basic authentication header and processes it
		 * accordingly based local user database.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next invoke to continue request handling
		 * @returns {void}
		 */
		static handleBasicAuth( req, res, next ) {
			if ( req.user ) {
				next();
				return;
			}

			const match = /^basic\s+([a-z0-9+/]+={1,2})$/i.exec( req.headers.authorization );
			if ( !match ) {
				next();
				return;
			}

			const decoded = Buffer.from( match[1], "base64" ).toString( "utf8" );
			const parts = /^([^:]+):(.+)$/.exec( decoded );
			if ( !parts ) {
				next();
				return;
			}

			service.AuthManager.checkAuthentication( parts[1], parts[2] )
				.then( user => {
					req.user = user;

					this.qualifyAuthenticated( req, res, next );
				} )
				.catch( next );
		}

		/**
		 * Authenticates a request, updates server-side session accordingly and
		 * injects information on authentication result in response header.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next invoke to continue request handling
		 * @returns {void}
		 */
		static login( req, res, next ) {
			const { strategy } = req.params;
			const { AuthenticationStrategies, AuthenticationPassport } = service;
			const defaultStrategy = AuthenticationStrategies.defaultStrategy();

			req.fetchBody()
				.then( body => {
					req.body = body;

					return new Promise( ( resolve, reject ) => {
						AuthenticationPassport.authenticate( strategy || defaultStrategy )( req, res, err => {
							if ( err ) {
								reject( err );
							} else {
								this.qualifyAuthenticated( req, res, error => {
									if ( error ) {
										reject( error );
									} else {
										resolve();
									}
								} );
							}
						} );
					} );
				} )
				.then( () => this.updateSessionId( req, res, next ) )
				.catch( err => {
					logAlert( err );

					AuthenticationPolicy.logout( req, res, cause => {
						if ( cause ) {
							logAlert( `applying logout policy after failed login has caused another issue: ${cause.stack}` );
						}

						next( err );
					} );
				} );
		}

		/**
		 * Extends request descriptor in case some authenticated user has been
		 * found recently.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next invoke to continue request handling
		 * @returns {void}
		 */
		static qualifyAuthenticated( req, res, next ) {
			if ( req.user ) {
				const { uuid, name } = req.user;

				service.AuthManager.listRolesOfUser( new models.User( uuid ) )
					.then( roles => {
						req.user.roles = roles;

						logDebug( "authenticated as", req.user.name );

						res.set( "X-Authenticated-As", name );
						res.set( "X-Authorized-As", roles.join( "," ) );

						next();
					} )
					.catch( next );
			} else {
				AuthenticationPolicy.logout( req, res, next );
			}
		}

		/**
		 * Drops any existing authentication on current request's user.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next invoke to continue request handling
		 * @returns {void}
		 */
		static logout( req, res, next ) {
			Promise.resolve()
				.then( () => {
					// (optional) log out remotely using current user's authentication strategy
					if ( req.user ) {
						const strategyName = req.user.strategy;

						if ( strategyName ) {
							const strategy = api.config.auth.strategies[strategyName];

							if ( strategy && typeof strategy.logOutRemotely === "function" ) {
								return strategy.logOutRemotely( req );
							}
						}
					}

					return undefined;
				} )
				.then( async willLogoutInFuture => {
					if ( !willLogoutInFuture ) {
						if ( typeof req.logout === "function" ) {
							await new Promise( ( resolve, reject ) => {
								req.logout( error => ( error ? reject( error ) : resolve() ) );
							} );
						} else {
							// re-generating session on change of user authentication mitigates session-related attacks
							// -> passport's logout does not seem to exist, so we can not rely on passport regenerating the session
							await new Promise( ( resolve, reject ) => req.session.regenerate( error => ( error ? reject( error ) : resolve() ) ) );
						}

						req.user = undefined;

						res.set( "X-Authenticated-As", undefined );
						res.set( "X-Authorized-As", undefined );
					}

					this.updateSessionId( req, res, next );
				} )
				.catch( next );
		}

		/**
		 * Promotes current session's ID in case it has changed compared to what
		 * the request was providing.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next callback to invoke to continue processing a request
		 * @returns {void}
		 */
		static updateSessionId( req, res, next ) {
			const { Session } = service;

			const sid = req.session?.id;

			if ( sid != null ) {
				if ( sid !== Session.getSessionIdOfRequest( req ) ) {
					// session might have been re-generated, hence its ID has
					// changed and we need to inform the client to use a
					// different session ID on next request
					req.session.$promoteSessionIdInResponse( res );
				}
			}

			next();
		}

		/**
		 * Ensures current request's user has authenticated.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next invoke to continue request handling
		 * @returns {void}
		 */
		static mustBeAuthenticated( req, res, next ) {
			if ( req.user ) {
				next();
			} else {
				res
					.status( 403 )
					.json( {
						error: "authentication required",
					} );
			}
		}
	}

	return AuthenticationPolicy;
}
