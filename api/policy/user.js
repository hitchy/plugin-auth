export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { models } = api;

	return {
		/**
		 * Tests if current request is addressing current user.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next invoke to continue request handling
		 * @returns {void}
		 */
		mustBeMe: ( req, res, next ) => {
			const { uuid } = req.user || {};

			if ( uuid && uuid === req.params.uuid ) {
				next();
				return;
			}

			res.status( 403 ).json( {
				error: "access forbidden: this is not you"
			} );
		},

		/**
		 * Tests if current request is addressing anything but current user.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next invoke to continue request handling
		 * @returns {void}
		 */
		mustNotBeMe: ( req, res, next ) => {
			const { uuid } = req.user || {};

			if ( !uuid || uuid !== req.params.uuid ) {
				next();
				return;
			}

			res.status( 403 ).json( {
				error: "access forbidden: this is you"
			} );
		},

		/**
		 * Integrates passport and server-side session with provided request.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} res response manager
		 * @param {Hitchy.Core.ContinuationHandler} next invoke to continue request handling
		 * @returns {void}
		 */
		changePassword: async( req, res, next ) => {
			const currentUser = req.user;

			if ( !currentUser ) {
				res
					.status( 403 )
					.json( {
						error: "access forbidden: authentication as user is required",
					} );

				next( new Error( "authentication required" ) );
				return;
			}

			const { currentPassword, nextPassword } = await new Promise( resolve => {
				switch ( req.method ) {
					case "PATCH" :
					case "POST" :
						return req.fetchBody().then( body => {
							resolve( {
								currentPassword: body.current,
								nextPassword: body.next,
							} );
						} );

					default :
						return {};
				}
			} );

			if ( !currentPassword ) {
				res
					.status( 400 )
					.json( {
						error: "current password is missing",
					} );

				next( new Error( "current password must be provided" ) );
				return;
			}

			if ( !nextPassword ) {
				res
					.status( 400 )
					.json( {
						error: "next password is missing",
					} );

				next( new Error( "next password must be provided" ) );
				return;
			}

			const { User } = models;
			const { uuid } = currentUser;
			const user = new User( uuid );

			const validPassword = await user.verifyPassword( currentPassword );

			if ( !validPassword ) {
				res
					.status( 403 )
					.json( {
						error: "access forbidden: wrong password",
					} );

				next( new Error( "wrong password" ) );
				return;
			}

			await user.setPassword( nextPassword );
			await user.save();

			next();
		}
	};
}
