/**
 * Implements commonly useful policy handlers for testing a request's
 * authorizations.
 */
export default class AuthorizationPolicy {
	/**
	 * Ensures current request's user has administrative privileges.
	 *
	 * @param {Hitchy.Core.IncomingMessage} req request descriptor
	 * @param {Hitchy.Core.ServerResponse} res response manager
	 * @param {Hitchy.Core.ContinuationHandler} next invoke to continue request handling
	 * @returns {void}
	 */
	static mustBeAdmin( req, res, next ) {
		const { adminRole } = this.services.AuthManager;

		if ( req.user && Array.isArray( req.user.roles ) && req.user.roles.some( role => role.name === adminRole ) ) {
			next();
			return;
		}

		res
			.status( 403 )
			.json( {
				error: "access forbidden: must be admin",
			} );
	}

	static useCMP = false;
}
