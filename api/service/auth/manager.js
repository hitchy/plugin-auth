export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { models, services } = api;

	const logDebug = api.log( "hitchy:auth:debug" );

	/**
	 * Implements several helper methods meant to simplify user/role management.
	 */
	class AuthManager {
		/**
		 * Qualifies selected user by means of loading or creating it when
		 * providing a user's name, only.
		 *
		 * @param {string|Hitchy.Plugin.Auth.User} user a user's name or model instance
		 * @param {boolean} createIfMissing set true to create any missing user
		 * @returns {Promise<Hitchy.Plugin.Auth.User>} promises selected user properly set up in database
		 */
		static async asUser( user, createIfMissing = false ) {
			if ( !user ) {
				throw new TypeError( "missing user information" );
			}

			const { User } = models;
			const users = user instanceof User ? [user] : await User.find( {
				eq: { name: "name", value: user.name || user },
			} );

			if ( users.length === 1 ) {
				return users[0];
			}

			if ( users.length > 1 ) {
				throw new Error( "selecting user did not yield unique match" );
			}

			if ( !createIfMissing ) {
				throw new Error( "selecting user did not yield any match" );
			}

			const newUser = new User();

			newUser.name = user.name || user;

			return await newUser.save();
		}

		/**
		 * Qualifies selected role by means of loading or creating it when
		 * providing a role's name, only.
		 *
		 * @param {string|Hitchy.Plugin.Auth.Role} role a role's name or model instance
		 * @param {boolean} createIfMissing set true to create any missing user
		 * @returns {Promise<Hitchy.Plugin.Auth.Role>} promises selected role properly set up in database
		 */
		static async asRole( role, createIfMissing = false ) {
			if ( !role ) {
				throw new TypeError( "missing role information" );
			}

			const { Role } = models;

			if ( !( role instanceof Role ) ) {
				role = String( role );

				if ( !/^[a-z_]/i.test( role ) || /\s/.test( role ) ) {
					throw new TypeError( "missing role information" );
				}
			}

			const roles = role instanceof Role ? [role] : await Role.find( {
				eq: { name: "name", value: role.name || role },
			} );

			if ( roles.length === 1 ) {
				return roles[0];
			}

			if ( roles.length > 1 ) {
				throw new Error( "selecting role did not yield unique match" );
			}

			if ( !createIfMissing ) {
				throw new Error( "selecting role did not yield any match" );
			}

			const newRole = new Role();

			newRole.name = role.name || role;

			return await newRole.save();
		}

		/**
		 * Fetches list of provided user's roles.
		 *
		 * @param {string|Hitchy.Plugin.Auth.User} user user to assign role to (given by instance or by its name)
		 * @param {boolean} createIfMissing set true to create any missing user
		 * @param {boolean} uuidsOnly true to prevent associated users' instances being loaded
		 * @returns {Promise<Role[]>} promises list of (unloaded) roles assigned to selected user
		 */
		static async listRolesOfUser( user, createIfMissing = false, uuidsOnly = false ) {
			const { $uuid: userUuid } = await this.asUser( user, createIfMissing );
			const { Role, UserToRole } = models;

			const associations = await UserToRole.find( { eq: { name: "user", value: userUuid } } );
			const numAssociations = associations.length;
			const roles = new Array( numAssociations );

			for ( let i = 0; i < numAssociations; i++ ) {
				const { role: roleUuid } = associations[i];

				if ( uuidsOnly ) {
					roles[i] = new Role( roleUuid );
				} else {
					roles[i] = new Role( roleUuid ).load();
				}
			}

			return uuidsOnly ? roles : Promise.all( roles );
		}

		/**
		 * Searches local database for user's with a role selected by its name.
		 *
		 * @param {string|Hitchy.Plugin.Auth.Role} role name of role to search for or that role's instance
		 * @param {boolean} createIfMissing set true to create any missing role
		 * @param {boolean} uuidsOnly true to prevent associated users' instances being loaded
		 * @returns {Promise<User[]>} promises list of users with selected role
		 */
		static async listUsersOfRole( role, createIfMissing = false, uuidsOnly = false ) {
			const { $uuid: roleUuid } = await this.asRole( role, createIfMissing );
			const { User, UserToRole } = models;

			const associations = await UserToRole.find( { eq: { name: "role", value: roleUuid } } );
			const numAssociations = associations.length;
			const users = new Array( numAssociations );

			for ( let i = 0; i < numAssociations; i++ ) {
				const { user: userUuid } = associations[i];

				if ( uuidsOnly ) {
					users[i] = new User( userUuid );
				} else {
					users[i] = new User( userUuid ).load();
				}
			}

			return uuidsOnly ? users : Promise.all( users );
		}

		/**
		 * Assigns selected role to selected user.
		 *
		 * @param {string|Hitchy.Plugin.Auth.Role} role role to assign user to (given by instance or by its name)
		 * @param {string|Hitchy.Plugin.Auth.User} user user to assign role to (given by instance or by its name)
		 * @param {boolean} createIfMissing set true to create any missing user/role
		 * @returns {Promise<void>} promises role assigned to user
		 */
		static async grantRoleToUser( role, user, createIfMissing = false ) {
			const { $uuid: userUuid } = await this.asUser( user, createIfMissing );
			const { $uuid: roleUuid } = await this.asRole( role, createIfMissing );

			const matches = await models.UserToRole.find( { eq: { name: "user", value: userUuid } } );
			const numMatches = matches.length;

			for ( let i = 0; i < numMatches; i++ ) {
				if ( matches[i].role.equals( roleUuid ) ) {
					return;
				}
			}

			const association = new models.UserToRole();
			association.user = userUuid;
			association.role = roleUuid;

			await association.save();
		}

		/**
		 * Drops assignment of selected role and selected user.
		 *
		 * @param {string|Hitchy.Plugin.Auth.Role} role role to assign user to (given by instance or by its name)
		 * @param {string|Hitchy.Plugin.Auth.User} user user to assign role to (given by instance or by its name)
		 * @param {boolean} createIfMissing set true to create any missing user/role
		 * @returns {Promise<void>} promises role assigned to user
		 */
		static async revokeRoleFromUser( role, user, createIfMissing = false ) {
			const { $uuid: userUuid } = await this.asUser( user, createIfMissing );
			const { $uuid: roleUuid } = await this.asRole( role, createIfMissing );

			const associations = await models.UserToRole.find( { eq: { name: "user", value: userUuid } } );
			const promises = [];

			for ( let i = 0, count = associations.length; i < count; i++ ) {
				const association = associations[i];

				if ( association.role.equals( roleUuid ) ) {
					promises.push( association.remove() );
				}
			}

			return Promise.all( promises );
		}

		/**
		 * Provides configurable name of role implicitly granting full access to
		 * its associated users.
		 *
		 * @returns {string} configured name of role
		 */
		static get adminRole() {
			const config = api.config.auth.admin;

			return process.env.HITCHY_ADMIN_ROLE || ( config ? config.role : "" ) || "admin";
		}

		/**
		 * Creates user with admin role if missing in database.
		 *
		 * @returns {Promise<Hitchy.Plugin.Auth.User[]>} promises list of eventually existing users with admin role
		 */
		static async createAdminUserIfMissing() {
			const role = await this.asRole( this.adminRole, true );
			const users = await this.listUsersOfRole( role );

			if ( users.length > 0 ) {
				logDebug( "admin user found" );

				return users;
			}


			const existingAdmins = await models.User.find( { eq: { name: "admin" } } );
			let user;

			if ( existingAdmins.length > 0 ) {
				user = existingAdmins[0];
			} else {
				const config = api.config.auth.admin;

				logDebug( "creating admin user" );

				user = new models.User();

				user.name = process.env.HITCHY_ADMIN_NAME || ( config ? config.name : "" ) || "admin";

				await user.setPassword( process.env.HITCHY_ADMIN_PASSWORD || ( config ? config.password : "" ) || "nimda" );
				await user.save();
			}

			await this.grantRoleToUser( role, user );

			return [user];
		}

		/**
		 * Checks if named user can be authenticated locally using provided
		 * password.
		 *
		 * @param {string} username name of user to authenticate
		 * @param {string} password password of named to user
		 * @return {Promise<User>} promises successfully authenticated user
		 */
		static async checkAuthentication( username, password ) {
			const candidates = ( await models.User
				.find( { eq: { name: username } }, {}, { loadRecords: true } ) )
				.filter( user => !user.strategy || user.strategy === "local" );

			switch ( candidates.length ) {
				case 0 :
					throw new services.HttpException( 400, `no such local user: ${username}` );

				case 1 : {
					const [user] = candidates;

					if ( await user.verifyPassword( password ) ) {
						return user;
					}

					throw new services.HttpException( 403, "invalid password" );
				}

				default :
					throw new services.HttpException( 400, "ambiguous username" );
			}
		}
	}

	return AuthManager;
}
