import { Strategy as LocalStrategy } from "passport-local";

/**
 * Temporarily tracks additional session data per remotely authenticated user.
 *
 * Some strategies (such as passport-saml) expect certain strategy-related data
 * per authenticated user to be present on `req.user` prior to accepting request
 * for logging out. In hitchy, `req.user` is fetched from database on every
 * request and thus no temporary data is available. Hence, this local map is
 * used instead to track any additional data for those strategies.
 *
 * @type {Map<any, any>}
 */
const RemoteAuthCustomData = new Map();

export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { models, services } = api;

	const logAlert = api.log( "hitchy:auth:alert" );

	/**
	 * Fetches named user's local profile.
	 *
	 * @param {string} strategyName name of strategy used to authenticate user
	 * @param {string} username name of user to search locally for related profile
	 * @param {boolean} createIfMissing set true to create user's profile if it's missing currenly
	 * @param {function(Error?, object):void} doneFn callback invoked with encountered error or user's profile
	 * @returns {void}
	 */
	function getLocalProfile( strategyName, username, createIfMissing, doneFn ) {
		models.User.find( { eq: { name: username } } )
			.then( candidates => candidates.find( user => user.strategy === strategyName ) )
			.then( match => {
				if ( match ) {
					return match;
				}

				if ( createIfMissing ) {
					const newUser = new models.User();
					newUser.name = username;
					newUser.strategy = strategyName;

					return newUser.save();
				}

				throw new TypeError( "selected user does not exist" );
			} )
			.then( profile => doneFn( null, profile ) )
			.catch( doneFn );
	}

	/**
	 * Implements helpers for generating strategies for passport.js.
	 */
	class AuthenticationStrategies {
		/**
		 * Picks user based on provided name and checks if provided password is
		 * matching or not.
		 *
		 * @param {string} username name of user to authenticate
		 * @param {string} password named user's password for authentication
		 * @param {function(Error?, object, object):void} done invoked with optional error, authenticated user or some message as feedback
		 * @returns {void}
		 */
		static checkAuthentication( username, password, done ) {
			models.User
				.find( { eq: { username } }, {}, { loadRecords: true } )
				.then( matches => {
					switch ( matches.length ) {
						case 0 :
							done( null, false, { message: "Incorrect username." } );
							return undefined;

						case 1 : {
							const [user] = matches;

							if ( user.strategy && user.strategy !== "local" ) {
								done( null, false, { message: "Authenticating this user requires different strategy." } );
								return undefined;
							}

							return user.verifyPassword( password ).then( result => {
								if ( result ) {
									done( null, user );
								} else {
									done( null, false, { message: "Incorrect password." } );
								}
							} );
						}

						default :
							done( null, false, { message: "Ambiguous username." } );
							return undefined;
					}
				} )
				.catch( err => {
					logAlert( err );
					done( err );
				} );
		}

		/**
		 * Generates local strategy authenticating user based on user model
		 * managed in local document-oriented database.
		 *
		 * @returns {Strategy} generated strategy for use with passport.js
		 */
		static generateLocal() {
			const strategy = new LocalStrategy( ( name, password, done ) => {
				services.AuthManager.checkAuthentication( name, password, done )
					.then( user => {
						done( null, user );
					} )
					.catch( error => {
						if ( error instanceof services.HttpException && Math.floor( error.statusCode / 100 ) === 4 ) {
							done( null, false, { message: error.message } );
						} else {
							done( error );
						}
					} );
			} );

			strategy.passwordRequried = true;

			return strategy;
		}

		/**
		 * Generates SAML strategy authenticating user against some remote IdP
		 * based on SAML v2.0 protocol.
		 *
		 * @param {string} strategyName name of resulting strategy in context of your application
		 * @param {Hitchy.Plugin.Auth.SamlConfig} config SAML protocol configuration
		 * @returns {Strategy} generated strategy for use with passport.js
		 */
		static async generateSaml( strategyName, config ) {
			const verifyLocalProfileOnLogin = ( req, userInfo, done ) => {
				RemoteAuthCustomData.set( `${strategyName}:${userInfo.nameID}`, { ...userInfo } );

				getLocalProfile( strategyName, userInfo.nameID, true, done );
			};

			const verifyLocalProfileOnLogout = ( req, userInfo, done ) => {
				getLocalProfile( strategyName, userInfo.nameID, false, done );
			};

			const { Strategy } = await import( "passport-saml" );
			const strategy = new Strategy( {
				...config,
				passReqToCallback: true,
			}, verifyLocalProfileOnLogin, verifyLocalProfileOnLogout );

			/**
			 * Triggers requesting user being logged out remotely.
			 *
			 * @param {Hitchy.Core.IncomingMessage} req request descriptor
			 * @returns {Promise<boolean>} promises indicator if request was redirected to come back after remote logout succeeded
			 */
			strategy.logOutRemotely = req => {
				if ( req.query.SAMLResponse ) {
					return Promise.resolve( false );
				}

				const res = req.context.response;

				return new Promise( ( resolve, reject ) => {
					const { user } = req;

					const remoteSessionKey = `${strategyName}:${user.name}`;

					if ( !user || user.strategy !== strategyName || !RemoteAuthCustomData.has( remoteSessionKey ) ) {
						resolve( {} );
					} else {
						// inject custom auth data into `req.user` as expected by passport-saml strategy
						const data = RemoteAuthCustomData.get( remoteSessionKey );

						for ( const name of Object.keys( data ) ) {
							user[name] = data[name];
						}

						user.nameID = user.name;

						if ( !user.nameID || !user.nameIDFormat ) {
							reject( new Error( "missing nameID and nameIDFormat of user required for requesting logout at IdP" ) );
						} else {
							// ask strategy for generating logout URL for redirecting client to
							strategy.logout( req, ( error, logoutUrl ) => {
								if ( error ) {
									reject( error );
								} else {
									resolve( { name: remoteSessionKey, url: logoutUrl } );
								}
							} );
						}
					}
				} )
					.then( ( { name, url } ) => {
						if ( url ) {
							// first pass -> redirect to IdP
							res.redirect( 302, url );
							return true;
						}

						// second pass -> returned from IdP

						if ( name ) {
							RemoteAuthCustomData.delete( name );
						}

						return false;
					} );
			};

			Object.defineProperty( strategy, "$$doNotSeal$$", { value: true } );

			return strategy;
		}

		/**
		 * Creates strategy for illustrating and testing integration with remote IdP
		 * supporting OpenID Connect with Authorization Code Flow.
		 *
		 * @param {string} strategyName name of resulting strategy in context of your application
		 * @param {object} config OpenID Connect client configuration
		 * @returns {Promise<Strategy>} promises generated strategy for use with passport.js
		 */
		static async generateOpenIdConnect( strategyName, config ) {
			const verifyLocalProfileOnLogin = ( req, tokens, userInfo, done ) => {
				getLocalProfile( strategyName, userInfo.preferred_username, true, done );
			};

			const { Issuer, Strategy, generators } = await import( "openid-client" );
			const issuer = await Issuer.discover( config.discovery_url );
			const client = new issuer.Client( config );

			const strategy = new Strategy( {
				client,
				passReqToCallback: true,
			}, verifyLocalProfileOnLogin );

			strategy.logOutRemotely = req => {
				const key = `${strategyName}:logout_state`;

				if ( req.session[key] && req.query.state === req.session[key] ) {
					return Promise.resolve( false );
				}

				const state = req.session[key] = generators.state( 32 );

				// redirect user to discovered end_session_url of IdP
				req.context.response.redirect( 302, client.endSessionUrl( { state } ) );

				return Promise.resolve( true );
			};

			Object.defineProperty( strategy, "$$doNotSeal$$", { value: true } );

			return strategy;
		}

		/**
		 * Retrieves name of strategy to use by default.
		 *
		 * @returns {string} name of strategy to use for user authentication
		 */
		static defaultStrategy() {
			const { defaultStrategy, strategies } = api.config.auth;

			if ( defaultStrategy ) {
				return defaultStrategy;
			}

			const strategiesNames = Object.keys( strategies );

			return strategiesNames.length === 1 ? strategiesNames[0] : "local";
		}
	}

	return AuthenticationStrategies;
}
