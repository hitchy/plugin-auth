import PassportLib from "passport";

export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	const logAlert = api.log( "hitchy:auth:alert" );
	const logDebug = api.log( "hitchy:auth:debug" );

	const passport = new PassportLib.Passport();

	/**
	 * Integrates passport with this Hitchy-based application.
	 *
	 * @returns {void}
	 */
	passport.integrateWithHitchy = () => {
		const { models: { User }, services: { AuthManager } } = api;
		const { strategies } = api.config.auth;


		// set up passport to persist current user in server-side session
		passport.serializeUser( ( user, done ) => {
			logDebug( `serializeUser: { name: ${user.name}, uuid: ${user.uuid} }` );

			done( null, user.uuid );
		} );

		passport.deserializeUser( ( uuid, done ) => {
			/** @type {Hitchy.Plugin.Auth.User} */
			const user = new User( uuid );

			user.$exists
				.then( exists => {
					if ( exists ) return user.load();

					throw new Error( "missing user with selected UUID" );
				} )
				.then( () => AuthManager.listRolesOfUser( user ) )
				.then( roles => {
					user.roles = roles;

					logDebug( `still authenticated user: name: ${user.name}, uuid: ${user.uuid}, roles: ${roles.join( "," )}` );

					done( null, user );
				} )
				.catch( done );
		} );


		// pick configured passport authentication strategies to use
		const names = Object.keys( strategies );
		const numNames = names.length;

		for ( let i = 0; i < numNames; i++ ) {
			const name = names[i];
			const strategy = strategies[name];

			if ( strategy ) {
				try {
					passport.use( name, strategy );
				} catch ( error ) {
					logAlert( `using passport strategy ${name} failed:`, error );
				}
			}
		}
	};

	return passport;
}
