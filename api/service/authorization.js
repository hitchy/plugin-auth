export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { service } = api;

	/**
	 * Implements convenient helpers for inspecting a user's authorization based
	 * on configured hierarchy of authorization rules.
	 *
	 * @alias Authorization
	 */
	class AuthorizationService {
		/**
		 * Checks if described user is an administrator with privileged access.
		 *
		 * @param {User|string|undefined} user user to check, may be undefined if no user is known
		 * @returns {Promise<boolean>} promises result of check whether given user has privileged access or not
		 */
		static async isAdmin( user ) {
			const userInfo = user ? await service.AuthManager.asUser( user ).catch( () => undefined ) : undefined;
			const adminRole = service.AuthManager.adminRole;

			if ( adminRole && Array.isArray( userInfo?.roles ) ) {
				for ( let i = 0; i < userInfo.roles.length; i++ ) {
					if ( userInfo.roles[i]?.name === adminRole ) {
						return true;
					}
				}
			}

			return false;
		}

		/**
		 * Checks if given user may access named resource(s).
		 *
		 * @param {string|User} user user to be tested
		 * @param {string|string[]} resource name(s) of resource(s)
		 * @param {boolean} checkAll if true, user has to be granted access on all named resources
		 * @returns {Promise<boolean>} promise resolved with true if selected user may access (some or all of the) named resource(s)
		 */
		static async mayAccess( user, resource, checkAll = false ) {
			if ( !resource ) {
				throw new Error( "missing resources to test" );
			}

			const userInfo = user ? await service.AuthManager.asUser( user ).catch( () => undefined ) : undefined;
			const resources = Array.isArray( resource ) ? resource : [resource];
			const roleNames = Array.isArray( userInfo?.roles ) ? userInfo.roles.map( role => role.name ) : [];

			if ( roleNames.indexOf( service.AuthManager.adminRole ) > -1 ) {
				return true;
			}

			const { current: tree } = service.AuthorizationTree;

			for ( const current of resources ) {
				const isAuthorized = tree.isAuthorized( current, userInfo?.name, roleNames.length > 0 ? roleNames : undefined );

				if ( Boolean( isAuthorized ) !== Boolean( checkAll ) ) {
					// either: may access current one + does not require access on all => return true
					//     or: must not access current one + requires access on all => return false
					return !checkAll;
				}
			}

			return Boolean( checkAll );
		}
	}

	return AuthorizationService;
}
