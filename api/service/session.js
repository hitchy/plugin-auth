export default function( options, HitchyPluginSession ) { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	const logDebug = api.log( "hitchy:auth:debug" );
	const logError = api.log( "hitchy:auth:error" );

	/**
	 * Extends session implemented by `@hitchy/plugin-session` to provide
	 * additional methods used by passport as it is assuming to work with
	 * `express-session`.
	 */
	class PassportCompatibleSession extends HitchyPluginSession {
		#id;

		#user;

		#data;

		/**
		 * @param {string} sessionId unique ID of session
		 * @param {Object<string,any>} properties copy (!) of session's properties recovered from a session store
		 */
		constructor( sessionId, properties ) {
			super( sessionId, properties, false );

			this.#id = sessionId;
			this.#data = properties;

			Object.defineProperties( this, {
				/**
				 * Exposes ID of session.
				 *
				 * @name Session#id
				 * @property {string}
				 * @readonly
				 */
				id: { get: () => this.#id },

				/**
				 * Exposes authenticated user current session is associated with.
				 *
				 * @name Session#user
				 * @property {{uuid:string}}
				 */
				user: {
					get: () => this.#user,
					set: newUser => {
						if ( !newUser || typeof newUser !== "object" || !newUser.uuid ) {
							throw new Error( "invalid user descriptor rejected" );
						}

						if ( this.#user != null ) {
							if ( newUser.uuid === this.#user.uuid ) {
								return;
							}

							throw new Error( "invalid request for replacing user of current session" );
						}

						this.#user = Object.freeze( { ...newUser } );
					},
				},

				/**
				 * Exposes space for saving additional custom data in context of
				 * current session.
				 *
				 * @name Session#data
				 * @property {object}
				 * @readonly
				 */
				$data: { get: () => this.#data },
			} );

			// OIDC plugin for passport is using custom redirection that's
			// preventing session from being persisted in late policy
			// -> instantly persist any changes related to the OIDC integration
			this.on( "data-changed", ( [key] ) => {
				if ( key.startsWith( "oidc:" ) ) {
					logDebug( "instantly persisting session on setting %s", key );

					this.constructor.getStore().save( this.id, this.$data ).catch( cause => {
						logError( "persisting session failed:", cause.stack );
					} );
				}
			} );
		}

		/**
		 * Drops any current session and creates a new one.
		 *
		 * This method is used by passport.
		 *
		 * @param {function((Error|undefined)): void} doneFn callback invoked when session has been re-generated or some error occurred
		 * @returns {void}
		 */
		regenerate( doneFn ) {
			logDebug( "re-generating current user session" );

			const store = this.constructor.getStore();

			store.drop( this.#id )
				.then( () => store.create() )
				.then( sessionId => {
					if ( !sessionId ) {
						throw new Error( "failed to assign new session ID" );
					}

					return store.load( sessionId )
						.then( record => {
							if ( !record ) {
								throw new Error( "failed to recover new session record" );
							}

							this.#id = sessionId;
							this.#data = record;
							this.#user = null;

							logDebug( `new session ID is ${sessionId}` );
							doneFn();
						} );
				} )
				.catch( cause => {
					logError( "re-generating current user session failed:", cause.stack );

					doneFn( cause );
				} );
		}

		/**
		 * Explicitly triggers saving of current session to configured store.
		 *
		 * This method is used by passport.
		 *
		 * @param {function((Error|undefined)):void} doneFn callback invoked when session has been saved or some error occurred
		 * @returns {void}
		 */
		save( doneFn ) {
			const store = this.constructor.getStore();

			store.save( this.#id, this.#data )
				.then( () => doneFn() )
				.catch( doneFn );
		}
	}

	return PassportCompatibleSession;
}
