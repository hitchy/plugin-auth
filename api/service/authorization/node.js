export default function() { // eslint-disable-line jsdoc/require-jsdoc
	/**
	 * Implements behavior of a single node in a hierarchy of authorization
	 * rules.
	 *
	 * @property {?AuthorizationNode} parent refers to superordinated node
	 * @property {?string} name name of segment addressing this node in context of its parent
	 * @property {{accept: Object<string, number>, reject: Object<string, number>}} roles maps names of roles into number of rules requesting to accept/reject
	 * @property {{accept: Object<string, number>, reject: Object<string, number>}} users lists UUIDs of users to accept/reject in context of node
	 * @property {Object<string,AuthorizationNode>} children maps relative names into subordinated nodes
	 */
	class AuthorizationNode {
		/**
		 * @param {string} [segmentName] name of segment addressing this node in context of its parent node
		 * @param {AuthorizationNode} [parent] refers to parent node of this one
		 */
		constructor( segmentName, parent ) {
			Object.defineProperties( this, {
				parent: {
					value: parent || undefined,
					enumerable: false,
				},
				name: {
					value: segmentName || undefined,
					enumerable: false,
				},
			} );

			this.clear();
		}

		/**
		 * Drops all information previously stored in node due to adding rules.
		 *
		 * @returns {AuthorizationNode} fluent interface
		 */
		clear() {
			this.roles = { accept: {}, reject: {} };
			this.users = { accept: {}, reject: {} };
			this.children = {};

			return this;
		}

		/**
		 * Tracks named role to be accepted or rejected in context of current
		 * node.
		 *
		 * @param {string} roleName name of role to add
		 * @param {boolean} accept true if role is accepted, false if it's rejected
		 * @returns {AuthorizationNode} fluent interface
		 */
		addRole( roleName, accept = true ) {
			if ( roleName && typeof roleName === "string" && roleName.trim().length > 0 ) {
				const list = this.roles[accept ? "accept" : "reject"];

				list[roleName] = ( list[roleName] || 0 ) + 1;
			}

			return this;
		}

		/**
		 * Tracks user described by its name to be accepted or rejected in
		 * context of current node.
		 *
		 * @param {string} userName name of user to add
		 * @param {boolean} accept true if user is granted access on resource, false if it's revoked
		 * @returns {AuthorizationNode} fluent interface
		 */
		addUser( userName, accept = true ) {
			if ( userName && typeof userName === "string" && userName.trim().length > 0 ) {
				const list = this.users[accept ? "accept" : "reject"];

				list[userName] = ( list[userName] || 0 ) + 1;
			}

			return this;
		}

		/**
		 * Drops named role to be accepted or rejected in context of current
		 * node.
		 *
		 * @param {string} roleName name of role to drop
		 * @param {boolean} accept true if role is accepted, false if it's rejected
		 * @returns {AuthorizationNode} fluent interface
		 */
		removeRole( roleName, accept = true ) {
			if ( roleName && typeof roleName === "string" && roleName.trim().length > 0 ) {
				const list = this.roles[accept ? "accept" : "reject"];

				list[roleName] = Math.max( 0, ( list[roleName] || 1 ) - 1 );
			}

			return this;
		}

		/**
		 * Drops user described by its name to be accepted or rejected in
		 * context of current node.
		 *
		 * @param {string} userName name of user to add
		 * @param {boolean} accept true if role is accepted, false if it's rejected
		 * @returns {AuthorizationNode} fluent interface
		 */
		removeUser( userName, accept = true ) {
			if ( userName && typeof userName === "string" && userName.trim().length > 0 ) {
				const list = this.users[accept ? "accept" : "reject"];

				list[userName] = Math.max( 0, ( list[userName] || 1 ) - 1 );
			}

			return this;
		}

		/**
		 * Fetches subordinated node by its name optionally creating it in case
		 * it is missing.
		 *
		 * @param {string} name name of child to look up
		 * @param {boolean} addIfMissing true if child should be created if missing
		 * @returns {AuthorizationNode} found or created child node, undefined if missing
		 */
		getChild( name, addIfMissing = false ) {
			if ( name && typeof name === "string" ) {
				let child = this.children[name];

				if ( child == null && addIfMissing ) {
					child = this.children[name] = new this.constructor( name, this );
				}

				return child || undefined;
			}

			return undefined;
		}

		/**
		 * Tests if provided user and/or role is affected by current node.
		 *
		 * @param {Buffer|Buffer[]} userName name(s) of user(s) to check
		 * @param {string|string[]} roleName name(s) of role(s) to check
		 * @returns {number} >0 on accepting user(s)/role(s) explicitly, <0 on rejecting explicitly, ==0 on missing explicit impact on user(s)/role(s)
		 */
		isAuthorized( userName, roleName ) {
			const state = { accept: 0, reject: 0 };

			if ( this.users.accept?.["*"] > 0 || this.roles.accept?.["*"] > 0 ) {
				state.accept = 1;
			}

			if ( this.users.reject?.["*"] > 0 || this.roles.reject?.["*"] > 0 ) {
				state.reject = 1;
			}

			const users = Array.isArray( userName ) ? userName : userName ? [userName] : [];
			const roles = Array.isArray( roleName ) ? roleName : roleName ? [roleName] : [];

			for ( const [ names, rules, level ] of [ [ users, this.users, 3 ], [ roles, this.roles, 2 ] ] ) {
				for ( const name of names ) {
					if ( name && typeof name === "string" ) {
						const _name = name.trim();

						if ( _name !== "" ) {
							for ( const action of [ "accept", "reject" ] ) {
								if ( rules[action][_name] > 0 ) {
									state[action] = Math.max( state[action], level );
								}
							}
						}
					}
				}
			}

			if ( state.accept > 0 && state.reject === state.accept ) {
				throw new Error( `granting and revoking access on "${this.path()}" to/from user(s) "${userName || "<none>"}" and/or role(s) "${roleName || "<none>"}"` );
			}

			if ( state.accept > state.reject ) {
				return 1;
			}

			if ( state.accept < state.reject ) {
				return -1;
			}

			return 0;
		}

		/**
		 * Indicates if current node lacks any viable information.
		 *
		 * @returns {boolean} true if node has not any viable information affecting authorization checks
		 */
		isSpare() {
			for ( const list of [ this.users.accept, this.users.reject, this.roles.accept, this.roles.reject ] ) {
				for ( let i = 0, keys = Object.keys( list ), length = keys.length; i < length; i++ ) {
					if ( list[keys[i]] > 0 ) {
						return false;
					}
				}
			}

			for ( let i = 0, keys = Object.keys( this.children ), length = keys.length; i < length; i++ ) {
				const child = this.children[keys[i]];

				if ( child != null && !child.isSpare() ) {
					return false;
				}
			}

			return true;
		}

		/**
		 * Renders path of current node mostly for debugging and logging
		 * purposes.
		 *
		 * @returns {string} path name of current node
		 */
		path() {
			const prefix = this.parent && this.parent.path();

			return prefix ? prefix + "." + this.name : this.name;
		}

		/**
		 * Generates string describing current tree of authorizations in a
		 * human-readable form.
		 *
		 * @return {string} rendered description of current tree of authorizations
		 */
		dump() {
			// TODO implement code for dumping current node to log
			return "tbd.";
		}

		/**
		 * Transfer every valuable information to a clone of current node and
		 * return it.
		 *
		 * @returns {AuthorizationNode} clone of current node limited to currently valuable data, undefined if node is not required anymore
		 */
		gc() {
			const children = this.children;
			const childrenNames = Object.keys( children );
			const numChildren = childrenNames.length;
			let clone;

			for ( let i = 0; i < numChildren; i++ ) {
				const childName = childrenNames[i];
				const child = children[childName];

				if ( child ) {
					const cleared = child.gc();

					if ( cleared ) {
						if ( !clone ) {
							clone = new this.constructor( this.name, this.parent );
						}

						clone.children[childName] = cleared;
					}
				}
			}

			for ( const type of [ "roles", "users" ] ) {
				for ( const mode of [ "accept", "reject" ] ) {
					const list = this[type][mode];
					const keys = Object.keys( list );
					const numKeys = keys.length;

					for ( let i = 0; i < numKeys; i++ ) {
						const key = keys[i];
						const count = list[key];

						if ( count > 0 ) {
							if ( !clone ) {
								clone = new this.constructor( this.name, this.parent );
							}

							clone[type][mode][key] = count;
						}
					}
				}
			}

			return clone;
		}
	}

	return AuthorizationNode;
}
