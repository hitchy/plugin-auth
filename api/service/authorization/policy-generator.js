export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	const logError = api.log( "hitchy:plugin:auth:error" );

	/**
	 * Implements factory functions for policy handlers checking a requesting
	 * user's authorization.
	 */
	class AuthorizationPolicyGenerator {
		/**
		 * Generates policy handler testing a request's user for having any of
		 * the listed roles and instantly rejecting requests of users who do not.
		 * Request is also rejected of request's user is unknown.
		 *
		 * @param {string|string[]} roles lists roles to accept
		 * @returns {Hitchy.Core.RequestPolicyHandler} policy handler rejecting requests of users who does not have any of the listed roles
		 */
		static hasRole( roles ) {
			if ( !roles ) {
				throw new Error( "missing roles to test" );
			}

			const accepted = {};

			if ( Array.isArray( roles ) ) {
				for ( const role of roles ) {
					accepted[role] = true;
				}
			} else {
				accepted[String( roles )] = true;
			}

			return ( req, res, next ) => {
				if ( req.user && Array.isArray( req.user.roles ) ) {
					const { adminRole } = req.hitchy.services.AuthManager;
					const userRoles = req.user.roles;
					const numUserRoles = userRoles.length;

					for ( let i = 0; i < numUserRoles; i++ ) {
						const role = userRoles[i].name;

						if ( role === adminRole || accepted[role] ) {
							next();
							return;
						}
					}
				}

				res
					.status( 403 )
					.json( {
						error: "access forbidden",
					} );
			};
		}

		/**
		 * Generates policy handler testing a request's user for being
		 * authorized to access at least one of the listed resources and
		 * instantly rejecting requests of users who do not.
		 *
		 * @param {string|string[]} resource name(s) of resource(s)
		 * @returns {Hitchy.Core.RequestPolicyHandler} policy handler rejecting requests of users who must not access any of the named resources
		 */
		static mayAccess( resource ) {
			if ( !resource ) {
				throw new Error( "missing resources to test" );
			}

			const resources = Array.isArray( resource ) ? resource : [resource];

			return async function( req, res, next ) {
				try {
					if ( await this.service.Authorization.mayAccess( req.user, resources ) ) {
						next();
						return;
					}
				} catch ( cause ) {
					logError( "checking user's authorization has thrown -> rejecting access", cause );
				}

				res
					.status( 403 )
					.json( {
						error: "access forbidden",
					} );
			};
		}
	}

	return AuthorizationPolicyGenerator;
}
