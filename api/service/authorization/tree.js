export default function() { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;
	const { services, models } = api;

	const logAlert = api.log( "hitchy:auth:alert" );
	const logDebug = api.log( "hitchy:auth:debug" );

	let cachedTree;

	/**
	 * Implements hierarchy of authorization nodes for caching complex
	 * authorizations rules.
	 *
	 * @name AuthorizationTree
	 */
	class AuthorizationTree {
		/**
		 * Fetches root node of current tree used to cache authorizations at
		 * runtime.
		 *
		 * @returns {AuthorizationTree} tree of authorizations
		 */
		static get current() {
			if ( !cachedTree ) {
				cachedTree = new this();
			}

			return cachedTree;
		}

		/** */
		constructor() {
			Object.defineProperties( this, {
				node: { value: new services.AuthorizationNode(), configurable: true },
			} );

			this._gcCounter = 0;
		}

		/**
		 * Converts provided authorization selector into its sequences of names
		 * suitable for descending into hierarchy of authorization nodes.
		 *
		 * @param {string} selector raw selector addressing node of hierarchy
		 * @returns {string[]} sequence of segments each naming another child to pick on descending into hierarchy
		 */
		static selectorToSegments( selector ) {
			if ( !selector || typeof selector !== "string" || !/^(?:\s*[^.\s]+\s*\.)*\s*[^.\s]+\s*$/.test( selector ) ) {
				throw new TypeError( `invalid authorization selector: ${selector}` );
			}

			return selector.trim().split( /\s*\.\s*/ );
		}

		/**
		 * Descends into current tree's hierarchy looking for the node named by
		 * provided selector.
		 *
		 * @param {string} selector full-stop-separated list of names each selecting another child node
		 * @param {boolean} addIfMissing true if missing nodes should be added implicitly
		 * @param {function(AuthorizationNode, string, number, string[]): (false|undefined)} callback function invoked on every node while descending
		 * @returns {AuthorizationNode} found node, undefined on addressing missing node while disabling implicit addition
		 */
		selectNode( selector, addIfMissing = true, callback = undefined ) {
			const segments = this.constructor.selectorToSegments( selector );
			let node = this.node;

			for ( let i = 0, length = segments.length; node && i < length; i++ ) {
				const segment = segments[i];

				node = node.getChild( segment, addIfMissing );

				if ( node && typeof callback === "function" ) {
					if ( callback( node, segment, i, segments ) === false ) {
						break;
					}
				}
			}

			return node;
		}

		/**
		 * Drops all nodes of tree implicitly removing any previously added rule.
		 *
		 * @returns {AuthorizationTree} fluent interface
		 */
		clear() {
			this.node.clear();

			return this;
		}

		/**
		 * Adds authorization rule to this tree.
		 *
		 * @param {AuthorizationRule} rule rule to be added
		 * @returns {AuthorizationTree} fluent interface
		 */
		addRule( rule ) {
			const { selector, role, user, accept } = rule;

			logDebug( "adding authorization rule", { selector, role, user, accept } );

			const node = this.selectNode( selector );

			if ( role ) {
				node.addRole( role, accept );
			}

			if ( user ) {
				node.addUser( user, accept );
			}

			return this;
		}

		/**
		 * Removes authorization rule from this tree.
		 *
		 * @param {AuthorizationRule} rule rule to be removed
		 * @returns {AuthorizationTree} fluent interface
		 */
		removeRule( rule ) {
			const { selector, role, user, accept } = rule;

			logDebug( "removing authorization rule", { selector, role, user, accept } );

			const node = this.selectNode( selector, false );

			if ( node ) {
				if ( role ) {
					node.removeRole( role, accept );
				}

				if ( user ) {
					node.removeUser( user, accept );
				}

				if ( role || user ) {
					this.gc();
				}
			}

			return this;
		}

		/**
		 * Tests if selected authorization is granted to or revoked from
		 * provided user(s) and/or role(s).
		 *
		 * @param {string} selector selector of authorization, full-stop-separated sequence of names
		 * @param {string|string[]} user names(s) of zero or more users to check
		 * @param {string|string[]} roles names of zero or more roles to check
		 * @param {boolean} acceptByDefault fallback result ro return if neither rule is affecting selected user(s) or role(s)
		 * @returns {boolean} true if authorization is granted to user(s)/role(s), false if it's rejected
		 */
		isAuthorized( selector, user, roles, acceptByDefault = false ) {
			let result = 0;

			try {
				this.selectNode( selector, false, node => {
					const localResult = node.isAuthorized( user, roles, acceptByDefault );

					if ( localResult !== 0 ) {
						result = localResult;
					}
				} );
			} catch ( cause ) {
				logAlert( cause.message );
				return false;
			}

			if ( result > 0 ) {
				return true;
			}

			if ( result < 0 ) {
				return false;
			}

			return Boolean( acceptByDefault );
		}

		/**
		 * Implements occasional garbage collection.
		 *
		 * @protected
		 * @param {boolean} force set true to force garbage collection
		 * @returns {void}
		 */
		gc( force = false ) {
			if ( force || ++this._gcCounter % 100 === 0 ) {
				Object.defineProperty( this, "node", {
					value: this.node.gc() || new services.AuthorizationNode(),
					configurable: true,
				} );
			}
		}

		/**
		 * Loads authorization rules from database.
		 *
		 * @returns {Promise<void>} promises authorization rules loaded from database
		 */
		async loadFromDatabase() {
			const entries = await models.AuthorizationRule.list( { loadProperties: true } );

			if ( entries.length > 0 ) {
				const { User, Role } = models;
				const users = {};
				const roles = {};

				logDebug( `restoring ${entries.length} authorization rule(s) from database` );

				for ( const entry of entries ) {
					const userKey = entry.user ? entry.user.toString( "hex" ) : undefined;
					const roleKey = entry.role ? entry.role.toString( "hex" ) : undefined;

					const user = userKey ? users[userKey] || await new User( entry.user ).load() : undefined; // eslint-disable-line no-await-in-loop
					const role = roleKey ? roles[roleKey] || await new Role( entry.role ).load() : undefined; // eslint-disable-line no-await-in-loop

					if ( user ) {
						users[userKey] = user;
					}

					if ( role ) {
						roles[roleKey] = role;
					}

					if ( user || role ) {
						this.addRule( {
							selector: entry.selector,
							role: role ? role.name : undefined,
							user: user ? user.name : undefined,
							accept: entry.accept,
						} );
					}
				}
			}
		}

		/**
		 * Loads authorization rules from configuration.
		 *
		 * @param {{authorizations: object}} configuration auth configuration to load pre-defined authorizations from
		 * @returns {void}
		 */
		loadFromConfiguration( configuration ) {
			const { authorizations = {} } = configuration || {};

			logDebug( `loading authorization(s) from configuration` );

			const processLevel = ( source, prefix ) => {
				const names = Object.keys( source );

				for ( const name of names ) {
					const sanitized = String( name ).trim().replace( /^\.+|\.+$/g, "" );
					const path = prefix === "" ? sanitized : prefix + "." + sanitized;
					const value = source[name];

					for ( const key of [ "users", "roles", "grant", "revoke", "people", "" ] ) {
						const items = key ? value[key] : value;
						if ( items == null ) {
							continue;
						}

						const list = String( items ).trim().split( "," )
							.map( i => String( i ?? "" ).trim().replace( /([+-])\s*/, "$1" ) ) // remove whitespace here to mitigate ReDOS vulnerability in pattern below
							.filter( i => i != null && i !== "" );

						for ( const who of list ) {
							const [ all, mode, role, id ] = /^([+-]?)(@?)\s*([^\s@,+-][^\s@,]*)$/.exec( who ) || [];

							if ( all ) {
								const isGranting = mode ? mode !== "-" : key !== "revoke";
								const isRole = role || key === "roles";

								this.addRule( {
									selector: path,
									accept: isGranting,
									[isRole ? "role" : "user"]: id,
								} );
							} else {
								logAlert( `invalid user/role selector in ${key || "rule"} on ${name}` );
							}
						}
					}

					if ( value.sub && !Array.isArray( value.sub ) ) {
						processLevel( value.sub, path );
					}
				}
			};

			processLevel( authorizations, "" );
		}
	}

	return AuthorizationTree;
}
