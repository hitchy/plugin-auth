import { ignoreFolders } from "eslint-config-cepharum";

export default [
	...ignoreFolders( "coverage", "public" )
];
