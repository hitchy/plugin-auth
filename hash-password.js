#!/usr/bin/env node
import UserModel from "./api/model/user.js";

const chunks = [];

process.stdin.on( "data", chunk => chunks.push( chunk ) );
process.stdin.once( "end", () => {
	const input = Buffer.concat( chunks );

	UserModel.call( { services: {}, models: {} } )
		.methods.hashPassword( input )
		.then( password => {
			console.log( password ); // eslint-disable-line no-console
		} )
		.catch( console.error );
} );
