export default function( options, plugins ) { // eslint-disable-line jsdoc/require-jsdoc
	const api = this;

	const logWarning = api.log( "hitchy:session:warning" );

	const pluginApi = {
		configure() {
			const { strategies } = api.config.auth;

			if ( !strategies.local ) {
				strategies.local = api.services.AuthenticationStrategies.generateLocal();
			}
		},

		/**
		 * Sets up auth plugin at the end of application bootstrap.
		 *
		 * @returns {Promise<void>} promises plugin being set up
		 */
		async initialize() {
			const {
				services: { AuthorizationTree, AuthenticationPassport, AuthManager },
			} = api;
			const { current } = AuthorizationTree;

			AuthenticationPassport.integrateWithHitchy();

			current.loadFromConfiguration( api.config.auth );

			const roles = api.config.auth.roles;

			if ( Array.isArray( roles ) && roles.length > 0 ) {
				for ( let i = 0; i < roles.length; i++ ) {
					const roleName = roles[i];

					if ( typeof roleName === "string" && roleName && !/\s/.test( roleName.trim() ) ) {
						await AuthManager.asRole( roleName.trim(), true ); // eslint-disable-line no-await-in-loop
					} else {
						throw new TypeError( "invalid name of role in runtime configuration" );
					}
				}
			}

			return Promise.all( [
				AuthManager.createAdminUserIfMissing(),
				current.loadFromDatabase(),
			] );
		},

		policies() {
			const { config: { session, auth: { prefix } } } = this;

			const policies = {};

			if ( session.disable ) {
				logWarning( "server-side sessions are disabled, thus passport is not integrated for commonly handling all requests automatically" );

				policies["/"] = [
					"authentication.handleBasicAuth",
				];
			} else {
				policies["/"] = [
					"authentication.injectPassport",
					"authentication.handleBasicAuth",
				];
			}

			return prefix === false ? policies : {
				...policies,
				[`POST ${prefix}/login{/:strategy}`]: ["authentication.login"],
				[`GET ${prefix}/login/:strategy`]: ["authentication.login"],
				[`GET ${prefix}/logout`]: ["authentication.logout"],
				[`POST ${prefix}/password`]: ["user.changePassword"],
				[`PATCH ${prefix}/password`]: ["user.changePassword"],
			};
		},

		routes() {
			const { config: { auth: { prefix } } } = this;

			return prefix === false ? {} : {
				after: {
					[`POST ${prefix}/login{/:strategy}`]: "user.authenticate",
					[`GET ${prefix}/login/:strategy`]: "user.authenticate",
					[`GET ${prefix}/current`]: "user.getCurrent",
					[`GET ${prefix}/logout`]: "user.unauthenticate",
					[`POST ${prefix}/password`]: ["user.changePassword"],
					[`PATCH ${prefix}/password`]: ["user.changePassword"],
				},
			};
		},
	};

	if ( Object.keys( plugins ).some( name => plugins[name].role === "odm-provider" ) ) {
		// application is using @hitchy/plugin-odem-rest or some related drop-in
		// -> assure to be processed before that
		pluginApi.$meta.dependants = ["odm-provider"];
	}

	return pluginApi;
}
